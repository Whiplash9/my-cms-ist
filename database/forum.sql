-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 05 2016 г., 20:13
-- Версия сервера: 5.5.45
-- Версия PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `forum`
--

-- --------------------------------------------------------

--
-- Структура таблицы `Dialogs`
--

CREATE TABLE IF NOT EXISTS `Dialogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `send` int(11) NOT NULL,
  `receive` int(11) NOT NULL,
  `last_message_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `Group_forum`
--

CREATE TABLE IF NOT EXISTS `Group_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `Group_forum`
--

INSERT INTO `Group_forum` (`id`, `name`) VALUES
(2, 'Администратор'),
(3, 'Модератор'),
(4, 'Пользователь'),
(5, 'Заблокирован');

-- --------------------------------------------------------

--
-- Структура таблицы `Group_real`
--

CREATE TABLE IF NOT EXISTS `Group_real` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `Group_real`
--

INSERT INTO `Group_real` (`id`, `name`) VALUES
(1, 'Преподаватель'),
(2, 'Студент'),
(3, 'Родитель студента'),
(4, 'Зав. кафедры');

-- --------------------------------------------------------

--
-- Структура таблицы `Messages`
--

CREATE TABLE IF NOT EXISTS `Messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_author` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `text` text NOT NULL,
  `id_topic` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `News`
--

CREATE TABLE IF NOT EXISTS `News` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_mai` int(11) NOT NULL,
  `name` text NOT NULL,
  `text` text NOT NULL,
  `date` date NOT NULL,
  `img` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=655 ;

--
-- Дамп данных таблицы `News`
--

INSERT INTO `News` (`id`, `id_mai`, `name`, `text`, `date`, `img`) VALUES
(640, 69640, 'Сотрудники «РТ-Химкомпозит» проходят дополнительное обучение в МАИ', '<p>Курс&nbsp; является победителем конкурсного отбора, проведённого в&nbsp;рамках ведомственной целевой программы «Повышение квалификации инженерно-технических кадров на&nbsp;2015–2016&nbsp;годы»</p>', '2016-06-03', '/upload/resize_cache/iblock/a0e/210_0_1/dsc00025.jpg'),
(641, 69645, 'К Марсу — на электричке: Гарри Попов о будущем электроракетных двигателей', '<p>В&nbsp;интервью газете «Военно-промышленный курьер» Гарри Попов рассказал о&nbsp;том, почему на&nbsp;данный момент нет ничего более перспективного, чем дальнейшее развитие ЭРД.</p>', '2016-06-03', '/upload/resize_cache/iblock/128/210_0_1/dsc0302.jpg'),
(642, 69643, 'Команда МАИ приняла участие в концерте для учеников интернатов и детских домов', '<p>Сотрудники ЦАТУ и&nbsp;ЦТПО показали ребятам мастер-класс по&nbsp;управлению пенолётами и&nbsp;коптерами.</p>', '2016-06-02', '/upload/resize_cache/iblock/551/210_0_1/img_4956.jpg'),
(643, 69547, 'МАИ стал сопредседателем Ассоциации технических вузов России и Китая', '<p>С&nbsp;китайской стороны сопредседателем на&nbsp;последующие два года стал Северо-западный политехнический университет города Сиань.</p>', '2016-06-02', '/upload/resize_cache/iblock/256/210_0_1/img_2589.jpg'),
(644, 69644, 'Академики 10 стран обсудили на форуме в Минске вопросы тепломассообмена', '<p>В конце мая в столице Беларуси проходил XV Минский международный форум по тепломассообмену. Представители двух кафедр Московского авиационного института представили на суд научного сообщества результаты своих научных изысканий.</p>', '2016-06-02', '/upload/resize_cache/iblock/e47/210_0_1/img_4266.jpg'),
(645, 69449, 'Четвёртое место в компетенции чемпионата WorldSkills Russia занял студент МАИ', '<p>В компетенции «Фрезерные работы на&nbsp;станках с&nbsp;ЧПУ» Евгений Кобзев уступил всего лишь 5&nbsp;баллов занявшему третье место.</p>', '2016-06-01', '/upload/resize_cache/iblock/4cd/210_0_1/bezymyannyy.png'),
(646, 69447, 'Представительница МАИ стала первой вице-мисс московского конкурса красоты', '<p>II&nbsp;Международный открытый студенческий конкурс красоты «Мисс Ассоциации иностранных студентов Москва&nbsp;— 2016» состоялся 27&nbsp;мая.</p>', '2016-06-01', '/upload/resize_cache/iblock/755/210_0_1/inostrannye-studenty.jpg'),
(647, 69442, '«Полимерный ренессанс» в авиастроении: комментарий эксперта МАИ «Российской газете»', '<p>Директор Института менеджмента, экономики и&nbsp;социальных технологий Роман Голов рассказал о&nbsp;том, почему композитные материалы заменили стеклопластик.</p>', '2016-06-01', '/upload/resize_cache/iblock/8d0/210_0_1/20_suxoy_1000_d_850.jpg'),
(648, 69539, 'Выборы ректора МАИ назначены на&nbsp;16 июня', '<p>Кандидаты Дмитрий Козорез, Михаил Погосян и&nbsp;Вячеслав Шевцов были утверждены Аттестационной комиссией Министерства образования и&nbsp;науки Российской Федерации.</p>', '2016-05-31', '/upload/resize_cache/iblock/981/210_0_1/15_1.jpg'),
(649, 69405, 'Делегаты от&nbsp;МАИ обсудили вопросы развития студенческих объединений', '<p>В&nbsp;делегацию вошли и.о. проректора по&nbsp;молодёжной политике Николай Степанов, начальник Управления по&nbsp;молодёжной политике Алексей Данилин, председатель культурно-массовой комиссии профкома студентов и&nbsp;аспирантов Олег Игнатов и&nbsp;член волонтёрского центра МАИ Иван Григоренко.</p>', '2016-05-31', '/upload/resize_cache/iblock/a4c/210_0_1/novost-na-sayt-mai_s-26-po-28_05_2016_prdso_2016-v-ulyanovske_foto.jpg'),
(650, 69402, 'Маёвец занял призовое место на российском этапе олимпиады по штамповке и моделированию', '<p>Первое и&nbsp;второе места на&nbsp;международной олимпиаде «Технологическая подготовка производства» взяли студенты Омского государственного технического университета и&nbsp;Московского государственного технического университета имени Н.&nbsp;Э.&nbsp;Баумана соответственно.</p>', '2016-05-31', '/upload/resize_cache/iblock/28f/210_0_1/bezymyannyy.png'),
(651, 69345, '«Не из фантастики, а из будущего»: Константин Ковалёв о разработках МАИ на «АТОМЭКСПО»', '<p>Внедрение новейших разработок, которые Московский авиационный институт представит на&nbsp;открывшемся 30&nbsp;мая международном форуме, серьёзно повлияет на&nbsp;нашу повседневную жизнь.</p>', '2016-05-30', '/upload/resize_cache/iblock/48a/210_0_1/1464607160627695.jpg'),
(652, 69391, 'В МАИ обсудили подготовку инженеров медицинской техники', '<p>Конференция на&nbsp;тему «Актуальные проблемы подготовки инженерных кадров по&nbsp;направлению «Биотехнические системы и&nbsp;технологии в&nbsp;медицине» состоялась 25&nbsp;мая.</p>', '2016-05-30', '/upload/resize_cache/iblock/73b/210_0_1/6iwefz_vqpu-_-kopiya-_2_.jpg'),
(653, 69297, 'HeliRussia — 2016: итоги можно подвести, но точки ставить ещё рано', '<p>Международная выставка &laquo;HeliRussia&nbsp;&mdash; 2016&raquo; завершила свою работу. Три активных выставочных дня принесли маёвцам немало позитивных моментов: награды, победы, презентации, важные обсуждения и&nbsp;переговоры, внимание посетителей и&nbsp;коллег по&nbsp;цеху, демонстрация достижений и&nbsp;планы на&nbsp;будущее.</p>', '2016-05-28', '/upload/resize_cache/iblock/e1e/210_0_1/l6iec0cfyc.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `Parents`
--

CREATE TABLE IF NOT EXISTS `Parents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `link_student` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `Personal_messages`
--

CREATE TABLE IF NOT EXISTS `Personal_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_dialog` int(11) NOT NULL,
  `id_author` int(11) NOT NULL,
  `text` text NOT NULL,
  `date` datetime NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `Settings`
--

CREATE TABLE IF NOT EXISTS `Settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `show_email` int(11) NOT NULL,
  `show_tel` int(11) NOT NULL,
  `last_visit` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `Settings`
--

INSERT INTO `Settings` (`id`, `user_id`, `show_email`, `show_tel`, `last_visit`) VALUES
(1, 2, 1, 1, '2016-06-05 21:07:09'),
(2, 3, 1, 1, '2016-06-05 21:01:40');

-- --------------------------------------------------------

--
-- Структура таблицы `Students`
--

CREATE TABLE IF NOT EXISTS `Students` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `group` int(11) NOT NULL,
  `chief` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `Students`
--

INSERT INTO `Students` (`id`, `id_user`, `course`, `group`, `chief`) VALUES
(2, 2, 4, 223, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `Subjects`
--

CREATE TABLE IF NOT EXISTS `Subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Дамп данных таблицы `Subjects`
--

INSERT INTO `Subjects` (`id`, `name`) VALUES
(3, 'Информатика'),
(4, 'Методы и средства защиты информации'),
(5, 'Базы данных'),
(6, 'Физика'),
(7, 'Математика'),
(8, 'Алгоритмические языки и программирование'),
(9, 'История'),
(10, 'Теория информации'),
(11, 'Основы карьеры'),
(12, 'Иностранный язык'),
(13, 'Физическая культура'),
(14, 'Инженерная и компьютерная графика'),
(15, 'Культурология'),
(16, 'Объектно-ориентированное программирование'),
(17, 'Построение и анализ алгоритмов'),
(18, 'Технологии пространственного моделирования'),
(19, 'Социология'),
(20, 'Технология программирования'),
(21, 'Основы экономической теории'),
(22, 'Электротехника и электроника'),
(23, 'Компьютерное обеспечение математических вычислений'),
(24, 'Теория принятия решений'),
(25, 'Стандартизация и сертификация программных продуктов'),
(26, 'Правоведение'),
(27, 'Программирование в информационных системах'),
(28, 'Философия'),
(29, 'Архитектура ЭВМ и систем'),
(30, 'WEB-программирование'),
(31, 'Производственный менеджмент'),
(32, 'Экология'),
(33, 'Основы психологии'),
(34, 'Сети и телекоммуникации'),
(35, 'Моделирование систем'),
(36, 'Многоагентные системы'),
(37, 'Операционные системы'),
(38, 'Информационные технологии'),
(39, 'Проектирование программных продуктов'),
(40, 'Системное программное обеспечение');

-- --------------------------------------------------------

--
-- Структура таблицы `Teachers`
--

CREATE TABLE IF NOT EXISTS `Teachers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `Teachers`
--

INSERT INTO `Teachers` (`id`, `id_user`, `subject`) VALUES
(2, 3, 38);

-- --------------------------------------------------------

--
-- Структура таблицы `Topics`
--

CREATE TABLE IF NOT EXISTS `Topics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_name` varchar(255) NOT NULL,
  `id_author` int(11) NOT NULL,
  `messages` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `last_message_date` datetime NOT NULL,
  `views` int(11) NOT NULL,
  `closed` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `Users`
--

CREATE TABLE IF NOT EXISTS `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL COMMENT 'Логин',
  `password` varchar(255) NOT NULL COMMENT 'Пароль',
  `name` varchar(50) NOT NULL COMMENT 'Имя на форуме',
  `avatar_folder` varchar(255) DEFAULT NULL,
  `registration_date` datetime NOT NULL COMMENT 'Дата регистрации',
  `email` varchar(50) NOT NULL COMMENT 'E-mail',
  `phone_number` varchar(15) NOT NULL COMMENT 'Номер телефона',
  `age` int(11) NOT NULL COMMENT 'Возраст',
  `messages` int(11) NOT NULL COMMENT 'Сколько сообщений написано',
  `topics` int(11) NOT NULL COMMENT 'Сколько тем создано',
  `group_forum` int(11) NOT NULL COMMENT 'Группа на форуме(админ,модератор)',
  `group_real` int(11) NOT NULL COMMENT 'Группа в жизни(учитель,студент...)',
  `active` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `Users`
--

INSERT INTO `Users` (`id`, `login`, `password`, `name`, `avatar_folder`, `registration_date`, `email`, `phone_number`, `age`, `messages`, `topics`, `group_forum`, `group_real`, `active`) VALUES
(2, 'admin', '$2y$10$SzPACP4/IHCXeyvIBRyM0.JBOZaSiRRViLPuFhG0GUAbsUlEGlFi6', 'Кириллов Павел Андреевич', '0', '2016-06-05 20:53:46', 'dsdfsd@mail.ru', '123123123', 21, 0, 0, 2, 2, 1),
(3, 'zav', '$2y$10$/vuy7.UU7k1L6g.M69yWFO3o7vrfQaqcB1gh1Mke/TdPcH/gfuVVi', 'Павлов Виталий Юрьевич', '0', '2016-06-05 20:56:28', 'alj@mail.ru', '231231', 40, 0, 0, 2, 4, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
