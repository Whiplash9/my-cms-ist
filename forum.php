<?php
    session_start(); 
    include_once "inc/lib.inc.php";
    include_once "inc/config.inc.php";
    include "inc/handler.php";
    check_online();
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?=$title?></title>
	<link rel="stylesheet" type="text/css" href="resource/style_forum.css">
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>-->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
        <script src="/resource/scripts/jquery.min.js" type="text/javascript"></script>
        <script src="/resource/scripts/scripts.js"></script>
        
</head>
<body>
	<div class="wrapper">
            <header>
                <div class="header_1">
                    <nav>
                        <ul class="list">
                            <?php
                                if ($id == "lk" || $id == "reg" || $id == "new_topic" || $showtopic > 0)
                                    echo "<a href='/forum.php'><li class='first-button'><div class='icon_back'></div>НА ГЛАВНУЮ</li></a>";
                                else
                                    echo "<a href='/'><li class='first-button'><div class='icon_back'></div>К НОВОСТЯМ</li></a>";
                            ?>
                            <!--<a href="/"><li class="first-button"><div class="icon_home"></div>К новостям</li></a>-->
                            <!--<a href="/forum.php?id=reg"><li class="menu-button">Регистрация</li></a>-->
                            <!--<a href="/forum.php"><li class="menu-button">Вопросы</li></a>-->
                            <?php
                                if ($id != lk){
                                    $new_messages = mysqli_fetch_assoc(mysqli_query($link, "SELECT COUNT(*) FROM `Dialogs` WHERE `status` = 1 AND `receive` = '$_SESSION[id]'"));
                                    $new_messages = $new_messages['COUNT(*)'];
                                    if ($new_messages > 0)
                                        $new_messages = '<div class="menu-button-new-messages">'.$new_messages.'</div>';
                                    else
                                        $new_messages = ''; 
                                }
                                if ($_SESSION["vhod"])
                                    echo "<a href='/forum.php?id=lk'><li class='menu-button'>ЛИЧНЫЙ КАБИНЕТ $new_messages</li></a>";
                                $new_messages = '';
                            ?>
                            <!--<a href="/forum.php"><li class="menu-button">Авторизация</li></a>-->
                        </ul>
                    </nav> 
                    <div class="logo">
                        <!--<a href="/forum.php"><div class="home_button"><div class="icon_home"></div><i>ФОРУМ</i></div></a>-->
                        <img src="resource/images/image_gray.png" class="logo_img"></img>
                        
                    </div>
                </div>
            </header>   

            <div class="who_container">
                <div class="who">
                    <?php

                        if (!$_SESSION["vhod"])
                            echo "Добро пожаловать на форум, Гость! <a href='/forum.php?id=reg'><i>Регистрация</i></a> или Авторизация ==>";
                        else{
                            echo "Приветствуем, $_SESSION[name]";
                        }
                        if (!$_SESSION["vhod"])
                            echo<<<SHOW
                        <div class="auth_form">
                            <form  action="$_SERVER[REQUEST_URI]" method="POST">
                                <div class="auth_field_block"><input class="auth_field" type="text" name="auth_login" maxlength="15" placeholder="Логин" required>
                                <input class="auth_field" type="password" name="auth_password" maxlength="15" placeholder="Пароль" required></div>
                                <div class="auth_field_block"><label class="auth_checkbox"><input style="margin-right: 3px"  type="checkbox" name="remember_me">Запомнить меня</label> 
                                <input class="button auth_button" name="auth_submit" type="submit" value="Вход"> </div>
                            </form>
                        </div>
SHOW;
                        else{         
                            echo "<div class='auth_info'><form action=".$_SERVER[REQUEST_URI]." method='POST'>"; 
                            echo "<input class='button exit_button' name='exit_submit' type='submit' value='Выход'>";
                            echo "</form>";
                            echo "</div>";
                        }

                        ?> 
                </div>
            </div>
            <?php 
            showMessage();?>
	    <div class="content"> 
                    <?php 
                        if ($showtopic){
                            if ($page == 'topic_edit')
                                include "pages/topic_edit.php";
                            else if ($page == 'message_edit')
                                include "pages/message_edit.php";
                            else
                                include "pages/showtopic.php";
                        }
                        else{
                            switch($id){
                                case 'lk': 
                                        if ($_SESSION[vhod]){
                                            $check_active = mysqli_fetch_assoc(mysqli_query($link, "SELECT `active` FROM `Users` WHERE `id` = '$_SESSION[id]'"));
                                            if ($check_active[active] == 1){
                                                if ($lk_page == "edit")
                                                    include "pages/lk_edit.php";
                                                else if ($lk_page == "messages")
                                                    include "pages/messages.php";
                                                else if ($lk_page == "users_list")
                                                    include "pages/users_list.php";
                                                else if ($lk_page == "chief_page")
                                                    include "pages/chief_page.php";
                                                else if ($lk_page == "show_grades")
                                                    include "pages/show_grades.php";
                                                else if ($lk_page == "delete_account")
                                                    include "pages/delete_account.php";
                                                else if ($dialog > 0)
                                                    include "pages/dialog.php";
                                                else
                                                    include "pages/lk.php";
                                            }
                                            else
                                                echo "<div class='infoblock infoblock_lk'>Ваш аккаунт не активирован по e-mail!</div>";                             
                                        }

                                    break; 
                                case 'reg': 
                                    getNavigation("reg");
                                        include "pages/registration.php";
                                    break;
                                case 'new_topic':
                                    getNavigation("new_topic");
                                    include "pages/new_topic.php";
                                    break;
                                default:
                                    getNavigation("");
                                    echo '<div class="forum">';
                                    getTopics();
                                    echo "</div><a href='/forum.php?id=new_topic'><button class='button new_topic_button'>Создать новую тему</button></a>";
                                    break;   
                            }
                        }
                        
                    ?>        
	    </div>
            <?php
               include_once "inc/footer.inc.php";
            ?>	   
	</div>
	
</body>
<script type="text/javascript">
  var block = document.getElementById("block");
  block.scrollTop = block.scrollHeight;
</script>
</html>
