<div class="footer">
    <div class="footer_content">
        <div class="footer_about">
            <h4>О сайте</h4>
            <p>Сайт разработан в целях своевременного информирования родителей студентов кафедры ИСТ об успеваемости их детей.</p>
        </div>
        <div class="footer-info">
            <h4>Разделы сайта</h4>
            <ul>
                <a href="/index.php"><li>Новости</li></a>
                <a href="/forum.php"><li>Форум</li></a>
                <a href="/forum.php?id=lk"><li>Личный кабинет</li></a>
            </ul>
        </div>
        <div class="footer_end">
            Разработчик: студент 4 курса МАИ - Кириллов Павел<br /> <?=date('Y')?>
        </div>
    </div>
</div>
