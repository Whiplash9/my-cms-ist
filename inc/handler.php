<?php
//Обработка страниц
$title = '';
$id = strtolower(strip_tags(trim($_GET['id'])));
$showtopic = strtolower(strip_tags(trim($_GET['showtopic'])));
$page = strtolower(strip_tags(trim($_GET['page'])));
$user = strtolower(strip_tags(trim($_GET['user'])));
$lk_page = strtolower(strip_tags(trim($_GET['lk_page'])));
$q = strtolower(strip_tags(trim($_GET['q'])));
$f = strtolower(strip_tags(trim($_GET['f'])));
$dialog = strtolower(strip_tags(trim($_GET['dialog'])));
$message = strtolower(strip_tags(trim($_GET['message'])));
$code = strip_tags(trim($_GET['code']));

if ($showtopic){
    $sql = "SELECT `topic_name` FROM `Topics` WHERE `id` = ".$showtopic;
    $query = mysqli_query($link, $sql);
    $result = mysqli_fetch_assoc($query);
    if ($page == 'topic_edit')
        $title = "Редактировать тему";  
    else if ($page == 'message_edit')
        $title = "Редактировать сообщение";
    else if ($result != NULL)
        $title = $result["topic_name"];
    else
        $title = "Тема 1";
}
else{
    switch($id){
        case 'lk': 
            if ($lk_page == 'edit')
                $title = "Редактировать личные данные";
            else if ($lk_page == "messages")
                $title = "Мои сообщения";
            else if ($lk_page == "users_list")
                $title = "Поиск пользователей";
            else if ($lk_page == "chief_page")
                $title = "Функционал старосты";
            else if ($lk_page == "show_grades")
                $title = "Смотреть успеваемость";
            else if ($lk_page == "delete_account")
                $title = "Удалить аккаунт";
            else if ($dialog > 0)
                $title = "Диалог";
            else 
                $title = "Личный  кабинет";
            break;
        case 'reg': 
            $title = "Регистрация";
            break; 
        case 'new_topic': 
            $title = "Новая тема";
            break; 
        default:
            $title = "Форум";
            break;
    }
}

//Обновление новостей
if ((isset($_POST["submit_update_news"])) && ($_SERVER["REQUEST_METHOD"] == "POST")){
    parse('http://mai.ru/press/news/', '<div class="b-textbody j-marg2 col-xs-11 col-sm-7 col-md-8 ">', '<p class="text-muted">');   
    header('Location: /index.php');    
}

//Создание новости
if ((isset($_POST["submit_new_news"])) && ($_SERVER["REQUEST_METHOD"] == "POST")){
    $news_name = strip_tags(trim($_POST["new_news_name"]));
    $news_text = strip_tags(trim($_POST["new_news_text"]));
    $date = date("Y-m-d"); 
    
    $sql = "INSERT INTO `News` VALUES ('', 0, '$news_name', '$news_text', '$date', '')";
    $query = mysqli_query($link, $sql); 
    
    $sql = "SELECT `id` FROM `News` WHERE `name` = '$news_name'";
    $query = mysqli_query($link, $sql);
    $result_name = mysqli_fetch_assoc($query);
    
    //Загрузка фото для новости
    if ($_FILES['new_news_photo']['tmp_name']) {
        if ($_FILES['new_news_photo']['type'] != 'image/jpeg') 
            newMessage(1, 'Неверный тип изображения. Используйте jpeg', $_SERVER['REQUEST_URI']);
        if ($_FILES['new_news_photo']['size'] > 500000) 
            newMessage(1, 'Размер изображения слишком большой. Используйте максимум 0.5Мб.', $_SERVER['REQUEST_URI']);
        else{
            $Image = imagecreatefromjpeg($_FILES['new_news_photo']['tmp_name']);
            $Size = getimagesize($_FILES['new_news_photo']['tmp_name']);
            $Tmp = imagecreatetruecolor(230, 150);
            imagecopyresampled($Tmp, $Image, 0, 0, 0, 0, 230, 150, $Size[0], $Size[1]);
            $Files = glob('resource/news_photo/*', GLOB_ONLYDIR);
            foreach($Files as $Num => $Dir) {
                $Num++;
                $Count = sizeof(glob($Dir.'/*.*'));
                if ($Count < 250) {
                    $Download = $Dir.'/'.$result_name[id];
                    mysqli_query($link, "UPDATE `News` SET `img` = '$Num' WHERE `id` = ".$result_name[id]);
                    break;
                }
            }
            imagejpeg($Tmp, $Download.'.jpg');
            imagedestroy($Image);
            imagedestroy($Tmp);
        }
    }

    header('Location: /index.php');    
}


//Удаление новости
if ((isset($_POST["submit_delete_news"])) && ($_SERVER["REQUEST_METHOD"] == "POST")){
    $news_id = strip_tags(trim($_POST["news_id"]));
    
    $result_avatar_folder = mysqli_fetch_assoc(mysqli_query($link, "SELECT `img`, `id_mai` FROM `News` WHERE `id`= '$news_id'"));
    
    if ($result_avatar_folder['id_mai'] == 0 && $result_avatar_folder['img'] != NULL){
        $path = 'resource/news_photo/'.$result_avatar_folder['img'].'/'.$news_id.'.jpg';
        unlink($path);
    }
    
    $sql = "DELETE FROM `News` WHERE `id` = ".$news_id;
    $query = mysqli_query($link, $sql);

    header('Location: /index.php');    
}



//Авторизация
if ((isset($_POST["auth_submit"])) && ($_SERVER["REQUEST_METHOD"] == "POST")){
    $login = strip_tags(trim($_POST["auth_login"]));
    $password = strip_tags(trim($_POST["auth_password"]));
    //Проверяем есть ли пользователь в базе
    $sql = "SELECT `password` FROM `Users` WHERE `login`='$login'";

    $query = mysqli_query($link, $sql);
    if (!$query){
         newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
    }
    else{
        $str = "";
        $result = mysqli_fetch_assoc($query);

        if ($result == NULL)
            $str = "Wrong";
        else{
            $hash = $result["password"]; 

            if (password_verify($password, $hash)){
                $str = "OK";
            }
            else
                $str = "Wrong";
        }

        switch ($str){
            case "OK": 
                $_SESSION["vhod"] = true;
                $_SESSION["login"] = $login;
                //Получаем из базы остальные поля
                $sql = "SELECT `id`, `name`, `avatar_folder`, `registration_date`, `email`, `phone_number`, `age`, `messages`, `topics`, `group_forum`, `group_real`, `active` FROM `Users` WHERE `login`='$login'";

                $query = mysqli_query($link, $sql);
                if (!$query){
                    newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
                }
                else{
                    $result = mysqli_fetch_assoc($query);
                    $_SESSION[id] = $result["id"];
                    $_SESSION[name] = $result["name"];
                    $_SESSION[avatar_folder] = $result["avatar_folder"];
                    $_SESSION[registration_date] = $result["registration_date"];
                    $_SESSION[email] = $result["email"];
                    $_SESSION[phone_number] = $result["phone_number"];
                    $_SESSION[age] = $result["age"];
                    $_SESSION[messages] = $result["messages"];
                    $_SESSION[topics] = $result["topics"];
                    $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Group_forum` WHERE `id`=$result[group_forum]"));
                    $_SESSION[group_forum] = $result_group_forum[name];
                    $_SESSION[group_real] = $result["group_real"];                   
                    $sql = "SELECT `name` FROM `Group_real` WHERE `id`=$_SESSION[group_real]";              
                    $query = mysqli_query($link, $sql);
                    $result_group = mysqli_fetch_assoc($query);
                    $_SESSION[group_real] = $result_group['name'];
                    $_SESSION[active] = $result['active'];
                    if (isset($_POST[remember_me]))
                        setcookie ("user_cookie", $hash, strtotime("+30days"));
                }
                header('Location: '.$_SERVER['REQUEST_URI']);
                break;
            case "Wrong":
                newMessage (1, 'Неверный логин/пароль', $_SERVER[REQUEST_URI]);
                break;
            default:
                newMessage (1, 'Непредвиденная ошибка! Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
        }
    }
}

//Выход из аккаунта
if (isset($_POST["exit_submit"])  && ($_SERVER["REQUEST_METHOD"] == "POST")){
    $_SESSION["vhod"] = false;
    session_destroy();
    if ($id == "lk")
        exit(header('Location: /forum.php'));
    else
    {
        header('Location: '.$_SERVER['REQUEST_URI']);       
    }
}


//Регистрация   
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_reg"])) ){
    //Обработка всех введенных значений
    $login = strip_tags(trim($_POST["login"]));
    $password = strip_tags(trim($_POST["password"]));
    $name = strip_tags(trim($_POST["name"]));
    $date = date("Y-m-d H-i-s"); 
    $email = strip_tags(trim($_POST["email"]));
    $phone_number = strip_tags(trim($_POST["phone_number"]));
    $age = strip_tags(trim($_POST["age"]));
    $group_real = strip_tags(trim($_POST["group_real"]));
    $show_email = strip_tags(trim($_POST["show_email"]));
    $show_tel = strip_tags(trim($_POST["show_tel"]));
    $captcha = strip_tags(trim($_POST["captcha"]));
    
    if ($_SESSION['captcha'] != md5($captcha))
        newMessage (1, 'Неверный текст с картинки!', $_SERVER[REQUEST_URI]);
    
    //Проверка на занятый email
    $sql = "SELECT `email` FROM `Users` WHERE `email`='$email'";  
    $query = mysqli_query($link, $sql);
    $check_email = mysqli_fetch_assoc($query);
    if ($check_email != NULL)
            newMessage (1, 'Пользователь с таким email уже зарегистрирован!', $_SERVER[REQUEST_URI]);


    //Шифруем пароль
    $hash = password_hash($password, PASSWORD_BCRYPT);
    
    //Проверка на занятый логин
    $sql = "SELECT `login` FROM `Users` WHERE `login`='$login'";
    
    $query = mysqli_query($link, $sql);
    if (!$query){
        newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
    }
    else{
        $result = mysqli_fetch_assoc($query);

        if ($result != NULL)
            newMessage (1, 'Пользователь с таким логином уже существует!', $_SERVER[REQUEST_URI]);
        else {
                //Формируем и выполняем запрос на добавление пользователя в бд
                $sql = "INSERT INTO `Users` VALUES ('', '$login', '$hash', '$name', '0', '$date', '$email', '$phone_number', '$age', '0', '0', '4','$group_real', '0')";

                $result = mysqli_query($link, $sql);         
                if (!$result){
                    newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);      
                }
                else{
                    $code = str_replace('=', '', base64_encode($email));
                    mail($email, 'Регистрация в CMS_IST', 'Ваша ссылка для подтверждения аккаунта на портале для родителей студентов: '.$DOMAIN.$_SERVER[REQUEST_URI].'&page=activate&code='.substr($code, -6).substr($code, 0, -6)
                    .' Если вы не регистрировались на портале, проигнорируйте данное сообщение.', 'CMS_ИСТ');
                    $result_new_user_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `id` FROM `Users` WHERE `login`='$login'"));
                    $sql = "INSERT INTO `Settings` VALUES ('', '$result_new_user_info[id]', '$show_email', '$show_tel', '$date')";
                    $query = mysqli_query($link, $sql);
                    
                    if ($group_real == 2){
                        $course = strip_tags(trim($_POST["course"]));
                        $group = strip_tags(trim($_POST["group"]));
                        $sql = "INSERT INTO `Students` VALUES ('', $result_new_user_info[id],'$course','$group',0)";
                        $result = mysqli_query($link, $sql); 
                        
                        if (!$result){
                            newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);      
                        }
                    }
                    else if ($group_real == 1){
                        $id_subject = strip_tags(trim($_POST["subjects"]));
                        $sql = "INSERT INTO `Teachers` VALUES ('', $result_new_user_info[id], $id_subject)";
                        $result = mysqli_query($link, $sql); 
                        
                        if (!$result){
                            newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);      
                        }
                    }
                    newMessage(3, 'Регистрация прошла успешна! Ссылка для подтверждения аккаунта отправлена на почтовый ящик: '.$email, "/forum.php");
                }
        }
    }
}

//Создание новой темы
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_new_topic"])) ){
    $topic_name = strip_tags(trim($_POST["topic_name"]));
    $text = strip_tags(trim($_POST["text"]));
    $date = date("Y-m-d H-i-s"); 
    $id_author = $_SESSION[id];
    
    $sql = "INSERT INTO `Topics` VALUES ('', '$topic_name', '$id_author' , 1, '$date', '$date', 0, 0)";

    $query = mysqli_query($link, $sql);
    if (!$query){
        newMessage (1, 'Непредвиденная ошибка! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
    }
    else{
        //Получаем айди созданной темы
        $sql = "SELECT `id` FROM `Topics` WHERE `create_date`='$date' AND `id_author`='$id_author' AND `topic_name`='$topic_name'";
        $query = mysqli_query($link, $sql);      
        $result = mysqli_fetch_assoc($query);
        $id_topic = $result["id"];
        
        $sql = "INSERT INTO `Messages` VALUES ('', '$_SESSION[id]]', '$date' , '$text', '$id_topic')";

        $query = mysqli_query($link, $sql);
        if (!$query){
            newMessage (1, 'Непредвиденная ошибка! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
        }
        
        $sql = "UPDATE `Users` SET `topics` = `topics` + 1 WHERE `id` = '$_SESSION[id]'";
        $query = mysqli_query($link, $sql);
        if (!$query){
            newMessage (1, 'Непредвиденная ошибка! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
        }
        else
            $_SESSION[topics]++;
        
        $sql = "UPDATE `Users` SET `messages` = `messages` + 1 WHERE `id` = '$_SESSION[id]'";
        $query = mysqli_query($link, $sql);
        if (!$query){
            newMessage (1, 'Непредвиденная ошибка! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
        }
        else
           $_SESSION[messages]++;
        
    }
    header('Location: /forum.php');
}

//Новое сообщение
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_new_message"])) ){
    $text = strip_tags(trim($_POST["message_text"]));

    $date = date("Y-m-d H-i-s");
    
    $sql = "INSERT INTO `Messages` VALUES ('', '$_SESSION[id]', '$date' , '$text', '$showtopic')";

    $query = mysqli_query($link, $sql);
    if (!$query){
        newMessage (1, 'Непредвиденная ошибка! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
    }
    else {
       $sql = "UPDATE `Topics` SET `messages` = `messages` + 1, `last_message_date` = '$date' WHERE `id` = '$showtopic'";
       $query = mysqli_query($link, $sql);
        if (!$query){
            newMessage (1, 'Непредвиденная ошибка! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);
        }
        else{ 
            $sql = "UPDATE `Users` SET `messages` = `messages` + 1 WHERE `id` = '$_SESSION[id]'";
            $query = mysqli_query($link, $sql);
            $_SESSION[messages]++;
            header('Location: '.$_SERVER['REQUEST_URI']); 
        }
    }   
}


//Редактировать сообщение
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_message_edit"])) ){
    $message_edit_id = strip_tags(trim($_POST["message_edit_id"]));
    $text = strip_tags(trim($_POST["message_edit_text"]));
    
    $sql = "UPDATE `Messages` SET `text` = '$text' WHERE `id` = '$message_edit_id'";
    $query = mysqli_query($link, $sql);

    newMessage (3, ' Сообщение успешно изменено!', 'forum.php?showtopic='.$showtopic);

}

//Удалить сообщение 
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_delete_message"])) ){
    $sql = "DELETE FROM `Messages` WHERE `id` = ".$_POST['id_message']; 
    $query = mysqli_query($link, $sql);
    
    $sql = "UPDATE `Users` SET `messages` = `messages` - 1 WHERE `id` = ".$_POST['id_author'];
    $query = mysqli_query($link, $sql);
    
    $sql = "UPDATE `Topics` SET `messages` = `messages` - 1 WHERE `id` = ".$_POST['id_topic'];
    $query = mysqli_query($link, $sql);
    
    $sql = "SELECT `messages` FROM `Topics` WHERE `id` = ".$_POST['id_topic'];
    $query = mysqli_query($link, $sql);
    $result_count_messages = mysqli_fetch_assoc($query);
    if ($result_count_messages[messages] == 0){
        $sql = "DELETE FROM `Topics` WHERE `id` = ".$_POST['id_topic'];
        $query = mysqli_query($link, $sql);

        $sql = "UPDATE `Users` SET `topics` = `topics` - 1 WHERE `id` = ".$_POST['id_author'];
        $query = mysqli_query($link, $sql);
    }
    
    header('Location: '.$_SERVER['REQUEST_URI']);    
}

//Удалить тему
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_delete_topic"])) ){   
    $sql = "SELECT `messages`, `id_author` FROM `Topics` WHERE `id` = ".$_POST['id_topic'];
    $query = mysqli_query($link, $sql);
    $result_info_topic = mysqli_fetch_assoc($query);
    
    $sql = "UPDATE `Users` SET `topics` = `topics` - 1 WHERE `id` = ".$result_info_topic[id_author];
    $query = mysqli_query($link, $sql);
    
    for ($i = 0; $i < $result_info_topic[messages]; $i++){
        $sql = "SELECT `id`,`id_author` FROM `Messages` WHERE `id_topic` = ".$_POST['id_topic'];
        $query = mysqli_query($link, $sql);
        $result_message_info = mysqli_fetch_assoc($query);
        
        
        
        $sql = "UPDATE `Users` SET `messages` = `messages` - 1 WHERE `id` = ".$result_message_info[id_author];
        $query = mysqli_query($link, $sql);
        
        $sql = "UPDATE `Topics` SET `messages` = `messages` - 1 WHERE `id` = ".$_POST['id_topic'];
        $query = mysqli_query($link, $sql);
        
        $sql = "DELETE FROM `Messages` WHERE `id` = ".$result_message_info[id];
        $query = mysqli_query($link, $sql);       
    }
    
    $sql = "DELETE FROM `Topics` WHERE `id` = ".$_POST['id_topic'];
    $query = mysqli_query($link, $sql);


    newMessage (3, 'Тема удалена!', "/forum.php");
}

//Закрыть тему
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_close_topic"])) ){
    $sql = "UPDATE `Topics` SET `closed` = 1 WHERE `id` = ".$_POST['id_topic'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Открыть тему
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_open_topic"])) ){
    $sql = "UPDATE `Topics` SET `closed` = 0 WHERE `id` = ".$_POST['id_topic'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Перейти к редактированию темы
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_go_edit_topic"])) ){     
    header('Location: forum.php?showtopic='.$_POST['id_topic'].'&page=topic_edit');
}

//Редактирование темы
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_edit_topic"])) ){ 
    $new_topic_name = strip_tags(trim($_POST["topic_name"]));
    $sql = "UPDATE `Topics` SET `topic_name` = '$new_topic_name' WHERE `id` = ".$_POST['id_topic'];
    $query = mysqli_query($link, $sql);
    if (!$query){
           newMessage (1, 'Непредвиденная ошибка! ('.mysqli_error($link).') Обратитесь к администратору.', 'forum.php?showtopic='.$_POST['id_topic']);
    }
    else
           newMessage (3, 'Данные обновлены!', 'forum.php?showtopic='.$_POST['id_topic']);
    header('Location: forum.php?showtopic='.$_POST['id_topic']);
}


//Бан пользователя
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_user_ban"])) ){
    $sql = "UPDATE `Users` SET `Group_forum` = 5 WHERE `id` = ".$_POST['id_user'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Разблокировка пользователя
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_user_ban_off"])) ){
    $sql = "UPDATE `Users` SET `Group_forum` = 4 WHERE `id` = ".$_POST['id_user'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Назначить старостой
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_appoint_chief"])) ){
    $sql = "UPDATE `Students` SET `chief` = 1 WHERE `id` = ".$_POST['id_user'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Разжаловать старосту
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_disappoint_chief"])) ){
    $sql = "UPDATE `Students` SET `chief` = 0 WHERE `id` = ".$_POST['id_user'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Назначить администратором форума
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_appoint_admin"])) ){
    $sql = "UPDATE `Users` SET `group_forum` = 2 WHERE `id` = ".$_POST['id_user'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Назначить модератором форума
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_appoint_moderator"])) ){
    $sql = "UPDATE `Users` SET `group_forum` = 3 WHERE `id` = ".$_POST['id_user'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Лишить прав модератора/администратора форума
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_disappoint_admin"])) ){
    $sql = "UPDATE `Users` SET `group_forum` = 4 WHERE `id` = ".$_POST['id_user'];
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Удаление всех сообщений и тем пользователя
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_delete_all_user_messages"])) ){
    $sql = "DELETE FROM `Messages` WHERE `id_author` = ".$_POST['id_user'];//Удаляем все сообщения пользователя
    $query = mysqli_query($link, $sql);
    
    $sql = "SELECT `id` FROM `Topics` WHERE `id_author` = ".$_POST['id_user'];//Удаляем все сообщения из тем пользователя
    $query = mysqli_query($link, $sql);
    $result_id_topics = mysqli_fetch_assoc($query);
    
    if ($result_id_topics != NULL)
        foreach ($result_id_topics as $value){
            $sql = "DELETE FROM `Messages` WHERE `id_topic` = ".$value;
            $query = mysqli_query($link, $sql);
        }
        
    $sql = "DELETE FROM `Topics` WHERE `id_author` = ".$_POST['id_user'];//Удаляем все темы пользователя
    $query = mysqli_query($link, $sql);
    
    $sql = "UPDATE `Users` SET `messages` = 0, `topics` = 0 WHERE `id` = ".$_POST['id_user'];//Обновляем поля сообщений и тем пользователя в 0
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Изменение личных данных пользователя
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_lk_edit"])) ){
    $result_old_user_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name`, `email`, `phone_number`, `age` FROM `Users` WHERE `id`=$_SESSION[id]"));
    $result_settings = mysqli_fetch_assoc(mysqli_query($link, "SELECT `show_email`, `show_tel` FROM `Settings` WHERE `user_id`=$_SESSION[id]"));

    $new_password = strip_tags(trim($_POST["password"]));
    $new_password2 = strip_tags(trim($_POST["password_2"]));
    $new_FIO = strip_tags(trim($_POST["name"]));
    $new_email = strip_tags(trim($_POST["email"]));
    $new_number = strip_tags(trim($_POST["phone_number"]));
    $new_age = strip_tags(trim($_POST["age"]));
    $show_email = strip_tags(trim($_POST["edit_show_email"]));
    $show_tel = strip_tags(trim($_POST["edit_show_tel"]));

    if ($new_password != NULL){
        if ($new_password == $new_password2){
            $hash = password_hash($new_password, PASSWORD_BCRYPT);
            $sql = "UPDATE `Users` SET `password` = '$hash' WHERE `id` = ".$_SESSION['id'];
            $query = mysqli_query($link, $sql);
        }
        else
            newMessage (1, 'Введенные пароли не совпадают!', "/forum.php?id=lk&lk_page=edit");
    }
    if ($new_FIO != NULL && $new_FIO != $result_old_user_info[name]){
        $sql = "UPDATE `Users` SET `name` = '$new_FIO' WHERE `id` = ".$_SESSION['id'];
        $query = mysqli_query($link, $sql);
        if ($query){
           $_SESSION[name] = $new_FIO;
        }
    }
    if ($new_email != NULL && $new_email != $result_old_user_info[email]){
        $sql = "UPDATE `Users` SET `email` = '$new_email' WHERE `id` = ".$_SESSION['id'];
        $query = mysqli_query($link, $sql);
    }
    if ($new_number != NULL && $new_number != $result_old_user_info[phone_number]){
        $sql = "UPDATE `Users` SET `phone_number` = '$new_number' WHERE `id` = ".$_SESSION['id'];
        $query = mysqli_query($link, $sql);
    }
    if ($new_age != NULL && $new_age != $result_old_user_info[age]){
        $sql = "UPDATE `Users` SET `age` = '$new_age' WHERE `id` = ".$_SESSION['id'];
        $query = mysqli_query($link, $sql);
    }
    if ($show_email != $result_settings[show_email]){
        $sql = "UPDATE `Settings` SET `show_email` = '$show_email' WHERE `user_id` = ".$_SESSION['id'];
        $query = mysqli_query($link, $sql);
    }
    if ($show_tel != $result_settings[show_tel]){
        $sql = "UPDATE `Settings` SET `show_tel` = '$show_tel' WHERE `user_id` = ".$_SESSION['id'];
        $query = mysqli_query($link, $sql);
    }
    
    
    //Загрузка аватара
    if ($_FILES['avatar']['tmp_name']) {
        //var_dump($_FILES['avatar']['type']);
        if ($_FILES['avatar']['type'] != 'image/jpeg' && $_FILES['avatar']['type'] != 'image/png') 
            newMessage(1, 'Неверный тип изображения. Используйте jpeg/jpg/png', $_SERVER['REQUEST_URI']);
        if ($_FILES['avatar']['size'] > 20000) 
            newMessage(1, 'Размер изображения слишком большой. Используйте максимум 20Кб.', $_SERVER['REQUEST_URI']);
        else{
            if ($_FILES['avatar']['type'] == 'image/jpeg')
                $Image = imagecreatefromjpeg($_FILES['avatar']['tmp_name']);
            else if ($_FILES['avatar']['type'] == 'image/png')
                $Image = imagecreatefrompng($_FILES['avatar']['tmp_name']);
            $Size = getimagesize($_FILES['avatar']['tmp_name']);
            $Tmp = imagecreatetruecolor(150, 170);
            imagecopyresampled($Tmp, $Image, 0, 0, 0, 0, 150, 170, $Size[0], $Size[1]);
            if ($_SESSION[avatar_folder] == 0) {
                $Files = glob('resource/avatar/*', GLOB_ONLYDIR);
                foreach($Files as $Num => $Dir) {
                    $Num++;
                    $Count = sizeof(glob($Dir.'/*.*'));
                    if ($Count < 250) {
                        $Download = $Dir.'/'.$_SESSION[id];
                        $_SESSION[avatar_folder] = $Num;
                        mysqli_query($link, "UPDATE `Users` SET `avatar_folder` = '$Num' WHERE `id` = ".$_SESSION[id]);
                        break;
                    }
                }
                if ($_SESSION[avatar_folder] == 0)
                    newMessage(1, 'Невозможно обновить аватар. Обратитесь к администратору.', $_SERVER['REQUEST_URI']);
            }
            else 
                $Download = 'resource/avatar/'.$_SESSION[avatar_folder].'/'.$_SESSION[id];
            imagejpeg($Tmp, $Download.'.jpg');
            imagedestroy($Image);
            imagedestroy($Tmp);
        }
    }
    
    
    newMessage(3, 'Данные обновлены!', "/forum.php?id=lk&lk_page=edit");
}


//Поиск пользователей
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_lk_users_list"])) ){
    unset($_SESSION[users_list_course]);
    unset($_SESSION[users_list_group]);
    unset($_SESSION[users_list_subject]);
    
    $group_real = strip_tags(trim($_POST["group_real"]));
    $course = strip_tags(trim($_POST["course"]));
    $group = strip_tags(trim($_POST["group"]));
    $id_subject = strip_tags(trim($_POST["search_subjects"]));
    
    $_SESSION[users_list_group_real] = $group_real;
    
    if ($group_real == 2){
        $_SESSION[users_list_course] = $course;
        $_SESSION[users_list_group] = $group;
    } 
    else if ($group_real == 1){
        $_SESSION[users_list_subject] = $id_subject;
    }
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}



//Сохранение файла журнала
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_save_journal_file"])) ){   
    if ($_FILES['journal_file']['tmp_name']) {
        $ok = preg_match("/(vnd.openxmlformats-officedocument.spreadsheetml.sheet)+/u", $_FILES['attestation_file']['type']);
        if ($_FILES['journal_file']['type'] != 'application/vnd.ms-excel' && $_FILES['journal_file']['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && !$ok) 
            newMessage(1, 'Не верный тип файла. Использует файлы с расширением xlsx/xls', $_SERVER['REQUEST_URI']);
        if ($_FILES['journal_file']['size'] > 500000) 
            newMessage(1, 'Размер файла слишком большой! Используйте файлы размером не больше 500Кб.', $_SERVER['REQUEST_URI']);
        else{
            $number_group = strip_tags(trim($_POST["number_group"]));
            $number_week = strip_tags(trim($_POST["number_week"]));
            $table_name = "Journal_".$number_group;
            
            $sql = "SHOW TABLES LIKE '$table_name'";
            $query = mysqli_query($link, $sql);
            $result = mysqli_fetch_all($query);
            
            if ($result != NULL){
                $sql = "SELECT `week` FROM `$table_name` WHERE `week` = '$number_week' LIMIT 0, 1";
                $query = mysqli_query($link, $sql);
                $result = mysqli_fetch_assoc($query);

                if ($result != NULL)
                    newMessage(1, 'Журнал с указанной неделей уже загружен!', $_SERVER['REQUEST_URI']);             
            }
            
            $excel_arr = parse_excel_file($_FILES['journal_file']['tmp_name']);
            
            //проверка на правильность заполнения
            if ($excel_arr[8][B] != '№' || $excel_arr[2][D] != 'Дата занятий' || $excel_arr[5][B] != 'Дисциплины' || $excel_arr[1][AF] != 'Часов пропущено за неделю' || $excel_arr[9][C] == NULL)
                newMessage (1, 'Загружаемый журнал заполнен некорректно! Ознакомьтесь с примерами правильных заполнений ниже.', $_SERVER[REQUEST_URI]);

                
            $sql = "CREATE TABLE `$table_name` (`week` INT(11), `date` VARCHAR(255), `name` VARCHAR(255), `student_id` INT(11), `lesson_1` VARCHAR(255), `is_1` INT(11), `lesson_2` VARCHAR(255), `is_2` INT(11), `lesson_3` VARCHAR(255), `is_3` INT(11), `lesson_4` VARCHAR(255), `is_4` INT(11))";
            $query = mysqli_query($link, $sql);
            if (!query)
                newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);

            for ($i = 9; $excel_arr[$i][C] != NULL; $i++ ){
                $name = $excel_arr[$i][C];

                preg_match_all("/^[а-яА-Я]+/u", $name, $mas_surname);  //получаем фамилию из столбца ФИО 
                $surname = $mas_surname[0][0]; 

                preg_match_all("/\s+[а-яА-Я]{0,1}/u", $name, $mas_name_inicial);//получаем 1 букву имени из столбца ФИО 
                $name_inicial = substr($mas_name_inicial[0][0], 1);

                $pattern1 = $surname." ".$name_inicial;

                $sql = "SELECT Users.id FROM `Users` JOIN `Students` ON Users.id = Students.id_user WHERE Students.group = '$number_group' AND (Users.name LIKE '%$name%' OR Users.name LIKE '%$pattern1%')";
                $query = mysqli_query($link, $sql);
                $result = mysqli_fetch_assoc($query);

                if ($result[id] != NULL)
                    $student_id = $result[id];
                else  
                    $student_id = 0;

                insert_day($excel_arr, $table_name, $number_week, $name, $student_id, $i, 'F', 'G', 'H', 'I');
                insert_day($excel_arr, $table_name, $number_week, $name, $student_id, $i, 'J', 'K', 'L', 'M');
                insert_day($excel_arr, $table_name, $number_week, $name, $student_id, $i, 'N', 'O', 'P', 'Q');
                insert_day($excel_arr, $table_name, $number_week, $name, $student_id, $i, 'T', 'U', 'V', 'W');
                insert_day($excel_arr, $table_name, $number_week, $name, $student_id, $i, 'X', 'Y', 'Z', 'AA');
                insert_day($excel_arr, $table_name, $number_week, $name, $student_id, $i, 'AB', 'AC', 'AD', 'AE');            
            }
        }
    }
    
    
    newMessage(3, 'Журнал сохранен!', "forum.php?id=lk&lk_page=chief_page&q=1");
}


//Сохранение файла аттестации
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_save_attestation_file"])) ){   
    if ($_FILES['attestation_file']['tmp_name']) {
        $ok = preg_match("/(vnd.openxmlformats-officedocument.spreadsheetml.sheet)+/u", $_FILES['attestation_file']['type']);
        if ($_FILES['attestation_file']['type'] != 'application/vnd.ms-excel' && $_FILES['journal_file']['type'] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && !$ok) 
            newMessage(1, 'Не верный тип файла. Использует файлы с расширением xlsx/xls'.$_FILES['attestation_file']['type'], $_SERVER['REQUEST_URI']);
        if ($_FILES['attestation_file']['size'] > 500000) 
            newMessage(1, 'Размер файла слишком большой! Используйте файлы размером не больше 500Кб.', $_SERVER['REQUEST_URI']);
        else{
            $number_group = strip_tags(trim($_POST["number_group_at"]));
            $table_name = "Attestation_".$number_group;
            
            $excel_arr = parse_excel_file_many($_FILES['attestation_file']['tmp_name']);
            
            //проверка на правильность заполнения
            if ($excel_arr[0][3][B] != '№' || $excel_arr[0][6][B] != '1' || $excel_arr[0][1][B] != 'Аттестация' || $excel_arr[0][6][C] == NULL)
                newMessage (1, 'Загружаемая аттестация заполнена некорректно! Ознакомьтесь с примерами правильных заполнений ниже.', $_SERVER[REQUEST_URI]);

            
            $mas_letters = ['D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
            
            //Ищем дату в массиве
            for ($pos_letter = 0; $pos_letter < 23; $pos_letter++){
                if ($excel_arr[0][2][$mas_letters[$pos_letter]] != NULL){
                    $date = $excel_arr[0][2][$mas_letters[$pos_letter]];
                    break;
                }
            }
            
            $count_subject = 0;
            //Ищем количество предметов в массиве
            for ($pos_letter = 0; $pos_letter < 23; $pos_letter++){
                if ($excel_arr[0][4][$mas_letters[$pos_letter]] != NULL && $excel_arr[0][4][$mas_letters[$pos_letter]] != 'Итоги')
                    $count_subject++;
                if ($excel_arr[0][4][$mas_letters[$pos_letter]] == 'Итоги')
                    break;
            }
            
            
            
            $sql = "SHOW TABLES LIKE '$table_name'";
            $query = mysqli_query($link, $sql);
            $result = mysqli_fetch_all($query);
            
            if ($result != NULL){
                $sql = "SELECT `date` FROM `$table_name` WHERE `date` = '$date' LIMIT 0, 1";
                $query = mysqli_query($link, $sql);
                $result = mysqli_fetch_assoc($query);

                if ($result != NULL)
                    newMessage(1, 'Данная аттестация уже загружена!', $_SERVER['REQUEST_URI']);             
            }

            $sql = "CREATE TABLE `$table_name` (`date` VARCHAR(255), `student_name` VARCHAR(255), `student_id` INT(11), `teacher_name` VARCHAR(255), `teacher_id` INT(11), `lesson_name` VARCHAR(255), `progress` INT(11), `hours_miss` INT(11))";
            $query = mysqli_query($link, $sql);
            if (!query)
                newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.', $_SERVER[REQUEST_URI]);

            for ($i = 6; $excel_arr[0][$i][C] != NULL; $i++ ){
                //поиск и связь студента по базе 
                $student_name = $excel_arr[0][$i][C];

                preg_match_all("/^[а-яА-Я]+/u", $student_name, $mas_surname);  //получаем фамилию из столбца ФИО 
                $surname = $mas_surname[0][0]; 

                preg_match_all("/\s+[а-яА-Я]{0,1}/u", $student_name, $mas_name_inicial);//получаем 1 букву имени из столбца ФИО 
                $name_inicial = substr($mas_name_inicial[0][0], 1);

                $pattern1 = $surname." ".$name_inicial;

                $sql = "SELECT Users.id FROM `Users` JOIN `Students` ON Users.id = Students.id_user WHERE Students.group = '$number_group' AND (Users.name LIKE '%$student_name%' OR Users.name LIKE '%$pattern1%')";
                $query = mysqli_query($link, $sql);
                $result = mysqli_fetch_assoc($query);

                if ($result[id] != NULL)
                    $student_id = $result[id];
                else  
                    $student_id = 0;
                
                $pos_letter = 0;

                for ($k = 1; $k <= $count_subject; $k++){
                    $progress = $excel_arr[0][$i][$mas_letters[$pos_letter]];
                    $hours_miss = $excel_arr[0][$i][$mas_letters[$pos_letter+1]];
                    insert_subject($excel_arr, $table_name, $student_name, $student_id, $date, $k, $progress, $hours_miss);
                    $pos_letter += 2;
                }
            }
        }
    }
    
    
    newMessage(3, 'Аттестация сохранена!', "forum.php?id=lk&lk_page=chief_page&q=1");
}

//Просмотр журналов
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_chief_page_show_journals"])) ){
    unset($_SESSION[journal_name]);
    unset($_SESSION[journal_week]);
    
    $journal_name = strip_tags(trim($_POST["journal_name"]));
    $journal_week = strip_tags(trim($_POST["journal_week"]));
    
    $_SESSION[journal_name] = $journal_name;
    $_SESSION[journal_week] = $journal_week;
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}

//Удаление журнала
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_chief_page_delete_journal"])) ){
    unset($_SESSION[journal_name]);
    unset($_SESSION[journal_week]);
    $delete_journal_name = strip_tags(trim($_POST["journal_name"]));
    if ($_POST["journal_week"] != NULL){
        $delete_journal_week = strip_tags(trim($_POST["journal_week"]));
        $result_search_week = mysqli_fetch_assoc(mysqli_query($link, "SELECT DISTINCT `week` FROM `$delete_journal_name` WHERE `week`= '$delete_journal_week'"));
        if ($result_search_week == NULL)
            newMessage(1, 'Удаление невозможно. В выбранном журнале нет недели <b>'.$delete_journal_week.'</b> !', "forum.php?id=lk&lk_page=chief_page&q=2");
        else
            $sql = "DELETE FROM `$delete_journal_name` WHERE `week` = ".$delete_journal_week;//Удаляем определенную неделю
    }
    else
        $sql = "DELETE FROM `$delete_journal_name`";//Удаляем весь журнал
    $query = mysqli_query($link, $sql);
    
    $sql = "SELECT DISTINCT `week` FROM `$delete_journal_name`";//Проверка на пустой журнал
    $query = mysqli_query($link, $sql);
    $result = mysqli_fetch_assoc($query);
    
    if ($result == NULL){
        $sql = "DROP TABLE `$delete_journal_name`";//
        $query = mysqli_query($link, $sql);
    }
         
    newMessage(3, 'Журнал удален!', "forum.php?id=lk&lk_page=chief_page&q=2");
}

//Просмотр аттестации
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_chief_page_show_attestation"])) ){
    unset($_SESSION[attestation_name]);
    unset($_SESSION[attestation_date]);
        
    $attestation_name = strip_tags(trim($_POST["attestation_name"]));
    $attestation_date = strip_tags(trim($_POST["attestation_date"]));    

    
    $_SESSION[attestation_name] = $attestation_name;
    $_SESSION[attestation_date] = $attestation_date;
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}


//Удаление аттестации
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_chief_page_delete_attestation"])) ){
    unset($_SESSION[attestation_name]);
    unset($_SESSION[attestation_date]);
    $delete_attestation_name = strip_tags(trim($_POST["attestation_name"]));
    if ($_POST["attestation_date"] != NULL){
        $delete_attestation_date = strip_tags(trim($_POST["attestation_date"]));
        $result_search_date = mysqli_fetch_assoc(mysqli_query($link, "SELECT DISTINCT `date` FROM `$delete_attestation_name` WHERE `date` = '$delete_attestation_date'"));
        if ($result_search_date == NULL)
            newMessage(1, 'Удаление невозможно', "forum.php?id=lk&lk_page=chief_page&q=2&f=2");
        else
            $sql = "DELETE FROM `$delete_attestation_name` WHERE `date` = '$delete_attestation_date'";
    }
    else
        $sql = "DELETE FROM `$delete_attestation_name`";
    $query = mysqli_query($link, $sql);
    
    $sql = "SELECT DISTINCT `date` FROM `$delete_attestation_name`";
    $query = mysqli_query($link, $sql);
    $result = mysqli_fetch_assoc($query);
    
    if ($result == NULL){
        $sql = "DROP TABLE `$delete_attestation_name`";
        $query = mysqli_query($link, $sql);
    }
         
    newMessage(3, 'Аттестация удалена!', "forum.php?id=lk&lk_page=chief_page&q=2&f=2");
}


//Просмотр успеваемости
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_show_grades"])) ){
    unset($_SESSION[grades_type]);
    $_SESSION[show_grades] = 'Yes';
    
    $grades_type = strip_tags(trim($_POST["grades_type"]));
    
    if ($grades_type == 1){
        $_SESSION[grades_type] = 'Журнал';
    }
    else if ($grades_type == 2)
        $_SESSION[grades_type] = 'Аттестация';
    
    $link_student = strip_tags(trim($_POST["link_student"]));
    $_SESSION[link_student] = $link_student;
    
    header('Location: '.$_SERVER['REQUEST_URI']);
}


//Удаление аккаунта
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_delete_account"])) ){
    $delete_account_id = strip_tags(trim($_POST["delete_account_id"]));
    $delete_account_group_real = strip_tags(trim($_POST["group_real"]));
    
    if ($delete_account_group_real == 'Преподаватель'){
        $sql = "DELETE FROM `Teachers` WHERE `id_user` = '$delete_account_id'";
        $query = mysqli_query($link, $sql);
    }
    if ($delete_account_group_real == 'Студент'){
        $sql = "DELETE FROM `Students` WHERE `id_user` = '$delete_account_id'";
        $query = mysqli_query($link, $sql);
        
        $sql = "DELETE FROM `Parents` WHERE `link_student` = '$delete_account_id'";
        $query = mysqli_query($link, $sql);
    }
    if ($delete_account_group_real == 'Родитель студента'){
        $sql = "DELETE FROM `Parents` WHERE `id_user` = '$delete_account_id'";
        $query = mysqli_query($link, $sql);
    }
    //Удаление аватара
    $result_avatar_folder = mysqli_fetch_assoc(mysqli_query($link, "SELECT `avatar_folder` FROM `Users` WHERE `id`= '$delete_account_id'"));
    
    if ($result_avatar_folder[avatar_folder] != 0){
        $path = 'resource/avatar/'.$result_avatar_folder[avatar_folder].'/'.$delete_account_id.'.jpg';
        unlink($path);
    }
    
    //Удаление настроек
    $sql = "DELETE FROM `Settings` WHERE `user_id` = '$delete_account_id'";
    $query = mysqli_query($link, $sql);
    
    //Удаление основной информации
    $sql = "DELETE FROM `Users` WHERE `id` = '$delete_account_id'";
    $query = mysqli_query($link, $sql);
    
    $_SESSION["vhod"] = false;
    newMessage(3, 'Аккаунт удален!', 'forum.php');
    session_destroy();
}

//Перейти в диалог
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_go_dialog"])) ){
    $partner_id = strip_tags(trim($_POST["id_user"]));
    
    readMessages($partner_id);
    header('Location: forum.php?id=lk&dialog='.$partner_id);
}

//Отправить личное сообщение
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_new_personal_message"])) ){
    $personal_message_text = strip_tags(trim($_POST["personal_message_text"]));
    $id_dialog = strip_tags(trim($_POST["id_dialog"]));
    $send = strip_tags(trim($_POST["send"]));
    $receive = strip_tags(trim($_POST["receive"]));
    $date = date("Y-m-d H-i-s");
    
    if ($id_dialog == NULL){
        $sql = "INSERT INTO `Dialogs` VALUES ('', 1, '$send', '$receive', '$date')";
        $query = mysqli_query($link, $sql);
        
        $result_id_dialog = mysqli_fetch_assoc(mysqli_query($link, "SELECT `id` FROM `Dialogs` WHERE `send` = '$send' AND `receive` = '$receive'"));
        $id_dialog = $result_id_dialog[id];
        
    } 
    else if ($send != $_SESSION[id]){
        $sql = "UPDATE `Dialogs` SET `status` = 1, `send` = '$receive', `receive` = '$send', `last_message_date` = '$date'  WHERE `id` = '$id_dialog'";
        $query = mysqli_query($link, $sql);
    }
    else{
        $sql = "UPDATE `Dialogs` SET `status` = 1, `last_message_date` = '$date'  WHERE `id` = '$id_dialog'";
        $query = mysqli_query($link, $sql);
    }
    
    
    $sql = "INSERT INTO `Personal_messages` VALUES ('', '$id_dialog', '$_SESSION[id]', '$personal_message_text', '$date', 1)";
    $query = mysqli_query($link, $sql);
    
    header('Location: '.$_SERVER[REQUEST_URI]);
}
   
//Связать родителя и студента
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_create_relation"])) ){
    $parent_id = strip_tags(trim($_POST["parent_id"]));
    $id_relation_student = strip_tags(trim($_POST["relation_student"]));
    
    $check_student = mysqli_fetch_assoc(mysqli_query($link, "SELECT `link_student` FROM `Parents` WHERE `id_user` = '$parent_id' AND `link_student` = '$id_relation_student'"));
    
    if ($check_student != NULL)
        newMessage(1, 'Привязка к этому студенту уже выполнена!', $_SERVER[REQUEST_URI]);
    else{
        $sql = "INSERT INTO `Parents` VALUES ('', '$parent_id', '$id_relation_student')";
        $query = mysqli_query($link, $sql);

        newMessage(3, 'Привязка выполнена успешно!', $_SERVER[REQUEST_URI]);
    }
}

//Удалить связь
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_delete_relation"])) ){
    $parent_id = strip_tags(trim($_POST["parent_id"]));
    $id_relation_student = strip_tags(trim($_POST["delete_relation_student"]));

    $sql = "DELETE FROM `Parents` WHERE `id_user` = '$parent_id' AND `link_student` = '$id_relation_student' ";
    $query = mysqli_query($link, $sql);

    newMessage(3, 'Связь удалена успешно!', $_SERVER[REQUEST_URI]);
}

//Добавить предмет преподавателю
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_add_subject"])) ){
    $teacher_id = strip_tags(trim($_POST["teacher_id"]));
    $id_subject = strip_tags(trim($_POST["add_id_subject"]));
    
    $check_subject = mysqli_fetch_assoc(mysqli_query($link, "SELECT `subject` FROM `Teachers` WHERE `id_user` = '$teacher_id' AND `subject` = '$id_subject'"));
    
    if ($check_subject != NULL)
        newMessage(1, 'Выбранный предмет уже добавлен!', $_SERVER[REQUEST_URI]);
    else{
        $sql = "INSERT INTO `Teachers` VALUES ('', '$teacher_id', '$id_subject')";
        $query = mysqli_query($link, $sql);

        newMessage(3, 'Предмет добавлен успешно!', $_SERVER[REQUEST_URI]);
    }
}

//Удалить предмет преподавателя
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_delete_subject"])) ){
    $teacher_id = strip_tags(trim($_POST["teacher_id"]));
    $id_subject = strip_tags(trim($_POST["delete_id_subject"]));

    $sql = "DELETE FROM `Teachers` WHERE `id_user` = '$teacher_id' AND `subject` = '$id_subject' ";
    $query = mysqli_query($link, $sql);

    newMessage(3, 'Предмет удален успешно!', $_SERVER[REQUEST_URI]);
}

//Добавить предмет в бд
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_add_subject_to_db"])) ){
    $name_subject = strip_tags(trim($_POST["add_subject_name"]));
    
    $check_subject = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Subjects` WHERE `name` = '$name_subject'"));
    
    if ($check_subject != NULL)
        newMessage(1, 'Данный предмет уже есть в базе данных!', $_SERVER[REQUEST_URI]);
    else{
        $sql = "INSERT INTO `Subjects` VALUES ('', '$name_subject')";
        $query = mysqli_query($link, $sql);

        newMessage(3, 'Предмет добавлен успешно в базу данных!', $_SERVER[REQUEST_URI]);
    }
}

//Удалить предмет из бд
if ( ($_SERVER['REQUEST_METHOD'] == "POST") && (isset($_POST["submit_delete_subject_from_db"])) ){
    $id_subject = strip_tags(trim($_POST["delete_id_subject_from_bd"]));
    
    $sql = "DELETE FROM `Teachers` WHERE `subject` = '$id_subject'";
    $query = mysqli_query($link, $sql);

    $sql = "DELETE FROM `Subjects` WHERE `id` = '$id_subject'";
    $query = mysqli_query($link, $sql);
    
    newMessage(3, 'Предмет удален успешно!', $_SERVER[REQUEST_URI]);
}


//Подтверждение емэйл
if($page == 'activate' && $code && $id == 'reg'){
    $email = base64_decode(substr($code, 6).substr($code, 0, 6));
    if (strpos($email, '@') !== false) {
        mysqli_query($link, "UPDATE `Users`  SET `active` = 1 WHERE `email` = '$email'");
        $_SESSION['user_active_email'] = $email;
        if ($_SESSION['vhod']){
            $_SESSION[active] = 1;
        }
        newMessage(3, 'E-mail <b>'.$email.'</b> подтвержден.', '/forum.php');
    }
    else 
        newMessage(1, 'E-mail адрес не подтвержден.', '/forum.php');
}


?>
