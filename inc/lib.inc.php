<?php
    //показать новости
    function getNews(){
        global $link;
        echo "
        <div id='overlay'></div>
        <div id='new_news_block'>
            <div id='button_close'></div>
            <form class='new_news_form' action='$_SERVER[REQUEST_URI]' method='POST' enctype='multipart/form-data'>
                <br /><label>Тема новости<br /><input type='text' class='new_news_name' name='new_news_name' maxlength='100' required></label><br /><br />
                <label>Текст новости<br />
                <textarea class='new_news_textarea' maxlength='10000' name='new_news_text' required></textarea></label><br /><br />
                <label>Фотография новости (Не обязательно)<br \><input type='file' name='new_news_photo'></label><br /><br />
                <input type='submit' class='button topic_buttons' name='submit_new_news' value='Добавить новость'>
            </form>
        </div>";
        $sql = "SELECT COUNT(*) FROM `News`";
        $query = mysqli_query($link, $sql);
        $result = mysqli_fetch_assoc($query);
        $count_pages = ceil($result["COUNT(*)"]/20);
        $page = strtolower(strip_tags(trim($_GET['page'])));
        if ($page == NULL)
            $page = 1;
        
        $start_news = ($page - 1) * 20;
        
         
        $sql = "SELECT `id`, `id_mai`, `name`, `text`, `date`, `img` FROM `News` ORDER BY `date` DESC LIMIT $start_news, 20 ";
        
        $query = mysqli_query($link, $sql);
        
        if (!$query){
            newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.');
        }
        else{
            $result = mysqli_fetch_all($query);
            echo "
            <div class='news_buttons_block'>";
            if ($_SESSION["vhod"]){
                $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum`, `active` FROM `Users` WHERE `id`= '$_SESSION[id]'"));
                if ($result_group_forum[group_forum] == 2 && $result_group_forum[active] == 1){
                    echo"
                    <form class='form_with_head_buttons' action='$_SERVER[REQUEST_URI]' method='POST'>
                        <input type='button' class='button' id='new_news_button' name='' value='Создать новость'>
                        <input type='submit' class='button' name='submit_update_news' value='Обновить новости'>
                    </form>";
                }
            }
            if ($result != NULL)
                getNewsPages($page, $count_pages, "bottom", "nav_pages_top");
            echo"
            </div>";
            if ($result == NULL)
                echo "<p style='font-size: 18px; font-weight: bold; height: 400px; margin: 20px 0 0 20px;'>Новости не загружены!</p>";
            else{  


                for ($i = 0; $i < count($result); $i++ )
                {
                    $id = $result[$i][0];
                    $id_mai = $result[$i][1];
                    $name = $result[$i][2];
                    $text = $result[$i][3];
                    $date = $result[$i][4];
                    $date = checkMyDate($date, '');
                    $img = $result[$i][5];

                    echo "
                    <div class='news-block'>";
                        if ($_SESSION["vhod"]){
                            if ($result_group_forum[group_forum] == 2){
                                echo"
                                <form class='form_delete_message' action='$_SERVER[REQUEST_URI]' method='POST'>
                                    <input type='hidden' name='news_id' value='$id'>
                                    <input type='submit' class='news_delete' name='submit_delete_news' value='' title='Удалить новость'>
                                </form>";
                            }
                        }
                        echo"
                        <div class='news-photo'>";
                            if ($id_mai == 0 && $img != NULL)
                                echo"
                                <img src='resource/news_photo/$img/$id.jpg' alt='PHOTO' width='230px' height='150px'>";
                            else if ($img != NULL)
                                echo"
                                <a href='http://mai.ru/press/news/detail.php?ID=$id_mai' target='_blank'><img src='http://mai.ru$img' alt='PHOTO' width='230px' height='150px'></a>";
                            else
                                echo "Фото отсутствует";
                        echo"
                        </div>
                        <div class='news-text'>
                            <div class='news-info'>";
                                if ($id_mai == 0)
                                    echo"<span class='news_info_name'>$name</span><br>";
                                else
                                    echo"<a class='news_info_name' href='http://mai.ru/press/news/detail.php?ID=$id_mai' target='_blank'>$name</a><br>";
                            echo"
                                <span class='news-date'>$date</span>
                            </div>
                            $text
                        </div>";
                        if ($id_mai != 0)
                            echo "<div class='news-a'><a href='http://mai.ru/press/news/detail.php?ID=$id_mai' target='_blank'><div class='button news-button'>Узнать подробнее →</div></a></div>";
                    echo"
                    </div> ";    
                }
                if ($result != NULL){
                    echo "
                    <div class='news_buttons_block'>";
                        getNewsPages($page, $count_pages, "top", "nav_pages_top");
                    echo
                    "</div>";
                } 
                echo "
                    <a class='last_news_block_a' href='http://mai.ru/press/news/?PAGEN_1=2' target='_blank'>
                        <div class='news-block last_news-block'>
                            Узнать больше новостей на сайте МАИ...
                        </div>
                    </a>";      
            }  
        }      
   }
   
   //Вывод информации пользователю и перенаправление
    function newMessage($p1, $p2, $p3 = '') {
        if ($p1 == 1) {
                $p1 = 'Ошибка';
                $_SESSION['message'] = '<div class="MessageErrorBlock"><b>'.$p1.'</b>: '.$p2.'</div>';
        }
        else if ($p1 == 2) {
                $p1 = 'Подсказка';
                $_SESSION['message'] = '<div class="MessageHelpBlock"><b>'.$p1.'</b>: '.$p2.'</div>';
        }
        else if ($p1 == 3) {
                $p1 = 'Информация';
                $_SESSION['message'] = '<div class="MessageInfoBlock"><b>'.$p1.'</b>: '.$p2.'</div>';
        }
        
        if ($p3) {
            $_SERVER['HTTP_REFERER'] = $p3;
            exit(header('Location: '.$_SERVER['HTTP_REFERER']));
        }
    }
   
    function showMessage() {
        if ($_SESSION['message']){
            $Message = $_SESSION['message'];
        }
        echo $Message;
        $_SESSION['message'] = array();
    }
    
    //парсим нужную информацию(index.php)
    function parse($url, $start, $finish){
        global $link;
        
        $string = curl_get($url);
        $pos = strpos($string, $start);
        if ($pos == false)
            return "123";
        else{
            $str = substr($string, $pos);
            $result = substr($str, 0, strpos($str, $finish));
            preg_match_all("~[_][0-9]{5}[\"]~", $result, $mas_id);
            preg_match_all("~(?:/upload/).*(?:jpg|png|jpeg|gif|svg)~", $result, $mas_img);
            preg_match_all("~(?:h5>).*(?:ID=)[0-9]{5}(?:\">).*[a-zA-Zа-яА-Я0-9]+.*(?:</a>)~", $result, $mas_name);
            preg_match_all("~(?:<p>).*(?:</p>)~", $result, $mas_text);
            preg_match_all("~(?:b-date\">).*(?:</p>)~", $result, $mas_date);
            
            for ($i = 0; $i < count($mas_id[0]); $i++){  //обрабатываем массив айдишников новостей
                $mas_id[0][$i] = substr($mas_id[0][$i], 1);
                $mas_id[0][$i] = substr($mas_id[0][$i], 0, -1); 
            }
            
            for ($i = 0; $i < count($mas_name[0]); $i++){  //обрабатываем массив названий новостей
                $mas_name[0][$i] = substr($mas_name[0][$i], 33);
                $mas_name[0][$i] = substr($mas_name[0][$i], 0, -4);
            }
            
            for ($i = 0; $i < count($mas_date[0]); $i++){  //обрабатываем массив с датами
                $mas_date[0][$i] = substr($mas_date[0][$i], 8);
                $mas_date[0][$i] = substr($mas_date[0][$i], 0, -4); 
            }
            
            $sql = "DELETE FROM `News` WHERE `id_mai` != 0";
            $query = mysqli_query($link, $sql); 
            
            for ($i = 0; $i < count($mas_id[0]); $i++){  //заносим новости в базу
                $id = $mas_id[0][$i];
                $name = $mas_name[0][$i];
                $text = $mas_text[0][$i];
                $img = $mas_img[0][$i];
                $date = $mas_date[0][$i];
                
                $date = date_changer($date);
                $date = date_format($date, "Y-m-d");
                
                $sql = "INSERT INTO `News` VALUES ('','$id','$name','$text','$date', '$img')";
                $query = mysqli_query($link, $sql);         
            } 
            
            

        }
    }
    
    function date_changer($date){
        $Jan = preg_match("~.*(янв)~", $date);
        $Feb = preg_match("~.*(фев)~", $date);
        $Mar = preg_match("~.*(мар)~", $date);
        $Apr = preg_match("~.*(апр)~", $date);
        $May = preg_match("~.*(мая)~", $date);
        $Jun = preg_match("~.*(июн)~", $date);
        $Jul = preg_match("~.*(июл)~", $date);
        $Aug = preg_match("~.*(авг)~", $date);
        $Sep = preg_match("~.*(сен)~", $date);
        $Oct = preg_match("~.*(окт)~", $date);
        $Nov = preg_match("~.*(ноя)~", $date);
        $Dec = preg_match("~.*(дек)~", $date);
        
        $s = strpos($date, ' ');
        $day = substr($date, 0, -(strlen($date)-$s)); 
        $year = date("Y");
       
        if ($Jan)
            $my_date = $year.'/01/'.$day; 
        else if ($Feb)
            $my_date = $year.'/02/'.$day;
        else if ($Mar)
            $my_date = $year.'/03/'.$day;
        else if ($Apr)
            $my_date = $year.'/04/'.$day;
        else if ($May)
            $my_date = $year.'/05/'.$day;
        else if ($Jun)
            $my_date = $year.'/06/'.$day;
        else if ($Jul)
            $my_date = $year.'/07/'.$day;
        else if ($Aug)
            $my_date = $year.'/08/'.$day;
        else if ($Sep)
            $my_date = $year.'/09/'.$day;
        else if ($Oct)
            $my_date = $year.'/10/'.$day;
        else if ($Nov)
            $my_date = $year.'/11/'.$day;
        else if ($Dec)
            $my_date = $year.'/12/'.$day;
        
        $my_date = date_create($my_date);//переводим строку в объект времени
        return $my_date;
    }
    
    
    //получаем веб-страницу с помощью curl(lib.inc.php)
    function curl_get($url, $referer = 'http://www.google.com'){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.112 Safari/537.36');
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;      
    }
   
   //Показать темы форума (forum.php)
    function getTopics(){
        global $link;
        
        $sql = "SELECT `id`, `topic_name`, `id_author`, `messages`, `create_date`, `last_message_date`, `views` FROM `Topics` ORDER BY `last_message_date` DESC";
        $query = mysqli_query($link, $sql);
        
        if (!$query){
             newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.');
        }
        else{
            $result = mysqli_fetch_all($query);
        
                           
            echo "
            <table class='main-table' border='1'>
            <thead>
                <tr class='table_header'>
                    <th colspan='3'>Темы форума</th>
                </tr>
            </thead>";
            if ($result == NULL){
                echo "
                <col width='100%'>
                <tr><td style='text-align: center; padding: 50px;'>Форум пуст!</td></tr>";
            }
            else{
                echo"
                <col width='70%'>
                <col width='15%'>
                <col width='15%'>
                <tbody>";
                for ($i = 0; $i < count($result); $i++){
                    $id = $result[$i][0];
                    $topic_name = $result[$i][1];
                    $topic_id_author = $result[$i][2];
                    $messages = $result[$i][3];

                    $create_date = $result[$i][4];//Получаем дату в строке
                    $create_date = checkMyDate($create_date);
                    $last_message_date = $result[$i][5];//Получаем дату в строке
                    $last_message_date = checkMyDate($last_message_date);
                    $views = $result[$i][6];

                    //получаем имя автора темы по его айди
                    $sql = "SELECT `name` FROM `Users` WHERE `id`='$topic_id_author'";
                    $query = mysqli_query($link, $sql);
                    $result_name = mysqli_fetch_assoc($query);
                    $topic_name_author = $result_name["name"];


                    //Получаем информацию о последнем сообщении в теме 
                    $sql = "SELECT `id_author`,`create_date` FROM `Messages` WHERE `id_topic`='$id' ORDER BY `create_date` DESC LIMIT 1";
                    $query = mysqli_query($link, $sql);     
                    $result_message = mysqli_fetch_assoc($query);
                    $last_message_id_author = $result_message["id_author"];

                    //получаем имя автора последнего сообщения по его айди
                    $sql = "SELECT `name` FROM `Users` WHERE `id`='$last_message_id_author'";
                    $query = mysqli_query($link, $sql);
                    $result_name = mysqli_fetch_assoc($query);
                    $last_message_name_author = $result_name["name"];


                    echo "
                    <tr>
                    <td class='table_content'>
                        <div class='topic_name'><a href='/forum.php?showtopic=$id'>$topic_name</a><br/></div>
                        <span class='table_content_span'>Создатель: <a  style='font-weight: bold' href='forum.php?id=lk&user=$topic_id_author'>$topic_name_author</a> | $create_date</span>
                    </td>
                    <td class='table_info'>
                        <span class='table_info_span'>   
                        Всего сообщений: $messages <br/>
                        Всего просмотров: $views
                        </span>
                    </td>
                    <td class='table_last_message'>
                        <span class='table_info_span'>     
                        Последний ответ:<br/>
                        <a  style='color:#353535; font-weight: bold' href='forum.php?id=lk&user=$last_message_id_author'>$last_message_name_author</a><br/>
                        $last_message_date
                        </span>
                    </td>
                    </tr>";
                }
            }
            
            echo "
            </tbody>
            </table>";
        }
               
}

    //Показать сообщения в теме (showtopic.php)
    function getMessages($id_topic, $page=1){
        global $link;
        
        $sql = "UPDATE `Topics` SET `views` = `views` + 1 WHERE `id` = $id_topic";
        $query = mysqli_query($link, $sql);    
        $sql = "SELECT COUNT(*) FROM `Messages` WHERE `id_topic`='$id_topic'";
        $query = mysqli_query($link, $sql);
        $result = mysqli_fetch_assoc($query);
        $count_pages = ceil($result["COUNT(*)"]/20);
        if ($page > $count_pages)
            $page = 1;
        
        $start_message = ($page - 1) * 20;
        
         
        $sql = "SELECT `id`, `id_author`, `create_date`, `text` FROM `Messages` WHERE `id_topic`=$id_topic LIMIT $start_message, 20";
        
        $query = mysqli_query($link, $sql);
        
        if (!$query){
            newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.');
        }
        else{
            $result = mysqli_fetch_all($query);
            
            if ($result == NULL){
                getNavigation("showtopic");
                echo "<div class='infoblock infoblock_showtopic'>Данная тема удалена или еще не создана!</div>";
            }
            else{
                
                echo "<div class='topic_buttons_block'>";
                getNavigation("showtopic", $id_topic, $page, $count_pages);
                
                if ($_SESSION["vhod"]){
                    $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum`, `active` FROM `Users` WHERE `id`=$_SESSION[id]"));
                    $result_topic_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `id_author`, `closed` FROM `Topics` WHERE `id`=$id_topic"));
                    if ($result_group_forum[active] == 1 && $result_group_forum[group_forum] != 5 && ($result_group_forum[group_forum] == 2 || $result_group_forum[group_forum] == 3 || $result_topic_info[id_author] == $_SESSION[id])){
                        echo "<form class='form_with_head_buttons' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <input type='hidden' name='id_topic' value='$id_topic'>";
                                echo "<input type='submit' class='button topic_buttons' name='submit_go_edit_topic' value='Редактировать тему'>";
                                if ($result_topic_info[closed] == 0)
                                    echo "<input type='submit' class='button topic_buttons' name='submit_close_topic' value='Закрыть тему'>";
                                else 
                                    echo "<input type='submit' class='button topic_buttons' name='submit_open_topic' value='Открыть тему'>";
                                echo"
                                <input type='submit' class='button topic_buttons' name='submit_delete_topic' value='Удалить тему'>
                            </form>";
                    }
                }
                getNavigationPages($id_topic, $page, $count_pages, "bottom", false, "nav_pages_top");
                echo "</div>";
                for ($i = 0; $i < count($result); $i++ )
                {
                    $id = $result[$i][0];
                    $id_author = $result[$i][1];
                    $create_date = $result[$i][2];
                    $create_date = checkMyDate($create_date);
                    $text = $result[$i][3];
                    $count = (($page - 1) * 20) + 1 + $i;

                    $sql = "SELECT `name`, `avatar_folder`, `registration_date`, `messages`, `group_forum`, `group_real` FROM `Users` WHERE `id`=$id_author";                 
                    $query = mysqli_query($link, $sql);
        
                    if (!$query){
                        newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.');
                    }
                    else{
                        $result_author = mysqli_fetch_assoc($query);
                        
                        if ($result_author == NULL)
                            continue;
                        else{
                        $name = $result_author['name'];
                        $registration_date = $result_author['registration_date'];
                        $messages = $result_author['messages'];
                        $group_forum = $result_author['group_forum'];
                        $group_real = $result_author['group_real'];
                        
                    
                        $sql = "SELECT `name` FROM `Group_real` WHERE `id`=$group_real";
                    
                        $query = mysqli_query($link, $sql);
                        $result_group = mysqli_fetch_assoc($query);
                        $group_real = $result_group['name'];
                        echo "
                            <div class='message_block'>
                                <div class='user_info_block'>
                                    <div class='user_name'>
                                        <a  style='' href='forum.php?id=lk&user=$id_author'>$name</a>
                                    </div>
                                    ";
                                    if ($result_author[avatar_folder] != 0)
                                        echo "<div class='user_avatar' style='background-image: url(resource/avatar/$result_author[avatar_folder]/$id_author.jpg); background-size: cover;'>";
                                    else
                                        echo "<div class='user_avatar' style='background: url(resource/avatar/all.png); background-size: cover; background-position: center;'>";
                                    echo"
                                    <a href='forum.php?id=lk&user=$id_author' class='user_avatar_a'></a></div>";
                                    $online = show_online($id_author);
                                    if ($online == '<b>Online</b>')
                                        echo "<div class='show_online_block' >".$online."</div>";
                                    echo"    
                                    <hr />
                                    <div class='user_info'>
                                        <p>$group_real<br />Сообщений: $messages<br />Дата регистрации:<br /> $registration_date</p>
                                    </div>
                                </div>
                                <div class='message'>
                                    <div class='message_text_header'><span class='message_text_info'>Отправлено: $create_date</span><div class='message_count'> #$count</div> ";
                                            if ($result_group_forum[group_forum] == 2 || $result_group_forum[group_forum] == 3 || $id_author == $_SESSION[id])
                                                echo "
                                                    <div class='message_text_header_buttons_block'>
                                                                <form action='$_SERVER[REQUEST_URI]' method='POST'>
                                                                    <input type='hidden' name='id_message' value='$id'>
                                                                    <input type='hidden' name='id_author' value='$id_author'>
                                                                    <input type='hidden' name='id_topic' value='$id_topic'>
                                                                    <a href='forum.php?showtopic=$id_topic&page=message_edit&message=$id'><input type='button' class='button message_text_header_buttons' name='' value='Редактировать сообщение'></a>
                                                                    <input type='submit' class='button message_text_header_buttons' name='submit_delete_message' value='Удалить сообщение'>
                                                                </form>
                                                    </div> ";
                                        
                                    echo "
                                    </div>
                                    <div class='message_text'>
                                        $text
                                    </div>
                                </div>
                            </div>";    
                        }
                    }
                }
                getNavigationPages($id_topic, $page, $count_pages);
                
                if ($_SESSION["vhod"]){
                    if ($result_group_forum[active] != 1)
                        echo "<div class='infoblock'>Вы не можете оставлять сообщения. Ваш аккаунт не активирован по e-mail!</div>";
                    else if ($result_group_forum[group_forum] == 5)
                        echo "<div class='infoblock'>Вы не можете оставлять сообщения. Ваш аккаунт заблокирован!<br> Свяжитесь с администратором.</div>";
                    else if ($result_topic_info[closed] == 1)
                        echo "<div class='infoblock'>Вы не можете оставлять сообщения. Тема закрыта.</div>";
                    else{
                        echo "
                        <hr class='hr_under_messages'><br>
                        <div class='message_block''>
                            <div class='user_info_block' style='margin-top: 100px'>
                                <div class='user_name'>
                                    <a  style='color:#353535' href='forum.php?id=lk&user=$_SESSION[id]'>$_SESSION[name]</a>
                                </div>";
                                if ($_SESSION[avatar_folder] != 0)
                                    echo "<div class='user_avatar' style='background-image: url(resource/avatar/$_SESSION[avatar_folder]/$_SESSION[id].jpg); background-size: cover;'>";
                                else
                                    echo "<div class='user_avatar' style='background: url(resource/avatar/all.png); background-size: cover; background-position: center;'>";
                                echo"
                                <a href='forum.php?id=lk&user=$_SESSION[id]' class='user_avatar_a'></a></div> 
                            </div> 
                            <div class='message'>
                                <form class='new_message_form_block' action='$_SERVER[REQUEST_URI]' method='POST'>
                                    <label class='label_new_message'>Оставьте свое сообщение!<br>
                                    <textarea  class='new_message_textarea' maxlength='10000' name='message_text'  rows='20' required></textarea></label><br />
                                    <div class='new_message_buttons_block'>
                                        <input type='submit' class='button new_message_submit_button' name='submit_new_message' value='Отправить'>  
                                    </div>
                                </form>  
                            </div>
                        </div>";
                    }
                }
                else
                    echo "<div class='infoblock'>Пожалуйста, авторизуйтесь, чтобы оставлять сообщения.</div>";
            }
                
        }
     }
    
     
    //проверка даты и добавление слов сегодня,вчера (lib.inc.php)
    function checkMyDate($date, $time=', H:i:s') {
        //кол-во секунд в сутках        
        $my_time = time() - 86400; 
        //вчерашняя дата  
        $yesterday = date("Y-m-d", $my_time);
        $old_date = $date;

        $date = date_create($date);//переводим строку в объект времени

        if (date("Y-m-d") == date_format($date, "Y-m-d"))
            return date_format($date, "Сегодня".$time);
        else if ($yesterday == date_format($date, "Y-m-d"))
            return date_format($date, "Вчера".$time);
        else 
            return $old_date;
    }
    
    //отрисовка верхней навигации(forum.php и lib.inc.php)
    function getNavigation($url, $id_topic="", $page=1, $count_pages=1) {
        global $link;
        if ($url != 'lk') echo "<div class='navigation_block'>";
        switch ($url){
            case "showtopic": 
                $sql = "SELECT `topic_name` FROM `Topics` WHERE `id` = ".$id_topic;
                $query = mysqli_query($link, $sql);
                if ($query){
                    $result = mysqli_fetch_assoc($query);
                    $topic_name = $result["topic_name"];
                    echo "<div class='navigation'><a href='forum.php' class='nav_a'>Главная</a> → <a href='forum.php?showtopic=$id_topic' class='nav_a'>Тема №$id_topic($topic_name)</a></div>";
                }
                else
                    echo "<div class = 'navigation'><a href='forum.php' class='nav_a'>Главная</a> → Ошибка</div>";                    
                break;
            case "lk": 
                echo "<div class = 'navigation'><a href='forum.php' class='nav_a'>Главная</a> → <a href='forum.php?id=lk' class='nav_a'>Личный кабинет</a></div>";
                break;
            case "reg": 
                echo "<div class = 'navigation'><a href='forum.php' class='nav_a'>Главная</a> → <a href='forum.php?id=reg' class='nav_a'>Регистрация</a></div>";
                break;
            case "new_topic": 
                echo "<div class = 'navigation'><a href='forum.php' class='nav_a'>Главная</a> → <a href='forum.php?id=new_topic' class='nav_a'>Новая тема</a></div>";
                break;
            case "": 
                echo "<div class = 'navigation'><a href='forum.php' class='nav_a'>Главная</a></div>";
                break;
           
        }
        if ($url != 'lk') echo "</div>";
    }
    
    
    //отрисовка кнопок страничной навигации форума(lib.inc.php)
    function getNavigationPages($id_topic, $page, $count_pages, $vert_align = "top", $draw_nav_pages_block = true, $class = "nav_pages"){
        if ($draw_nav_pages_block)
            echo "<div class = 'nav_pages_block'>";
        echo "<ul class='$class'>";
            if ($page != 1){
                $prev_page = $page - 1;
                if ($page != 2)
                    echo "<a href='forum.php?showtopic=$id_topic&page=1'><li class='button nav_pages_button' style='vertical-align: $vert_align'><<</li></a>";
                echo "<a href='forum.php?showtopic=$id_topic&page=$prev_page'><li class='button nav_pages_button' style='vertical-align: $vert_align'>$prev_page</li></a>";
            }
            echo "<a href='forum.php?showtopic=$id_topic&page=$page'><li class='button nav_pages_button' style='background-color: #8fb488; vertical-align: $vert_align'>$page</li></a>";
            if ($page != $count_pages){
                $next_page = $page + 1;
                echo "<a href='forum.php?showtopic=$id_topic&page=$next_page'><li class='button nav_pages_button' style='vertical-align: $vert_align'>$next_page</li></a>";
                if ($page != $count_pages - 1)
                    echo "<a href='forum.php?showtopic=$id_topic&page=$count_pages'><li class='button nav_pages_button' style='vertical-align: $vert_align'>>></li></a>";
            }
        echo "</ul>";
        if ($draw_nav_pages_block)
            echo "</div>";
    }
    
    //отрисовка кнопок страничной навигации для новостей(lib.inc.php)
    function getNewsPages($page, $count_pages, $vert_align = "top", $class = "nav_pages"){
        echo "<ul class='$class'>";
            if ($page != 1){
                $prev_page = $page - 1;
                if ($page != 2)
                    echo "<a href='index.php?page=1'><li class='button nav_pages_button' style='vertical-align: $vert_align'><<</li></a>";
                echo "<a href='index.php?page=$prev_page'><li class='button nav_pages_button' style='vertical-align: $vert_align'>$prev_page</li></a>";
            }
            echo "<a href='index.php?page=$page'><li class='button nav_pages_button' style='background-color: #353535; vertical-align: $vert_align'>$page</li></a>";
            if ($page != $count_pages){
                $next_page = $page + 1;
                echo "<a href='index.php?page=$next_page'><li class='button nav_pages_button' style='vertical-align: $vert_align'>$next_page</li></a>";
                if ($page != $count_pages - 1)
                    echo "<a href='index.php?page=$count_pages'><li class='button nav_pages_button' style='vertical-align: $vert_align'>>></li></a>";
            }
        echo "</ul>";
    }
    
    function getLkMenu ($page){
        global $link;
        $new_messages = mysqli_fetch_assoc(mysqli_query($link, "SELECT COUNT(*) FROM `Dialogs` WHERE `status` = 1 AND `receive` = '$_SESSION[id]'"));
        $new_messages = $new_messages['COUNT(*)'];
        if ($new_messages > 0)
            $new_messages = '<div class="lk_menu_new_messages">'.$new_messages.'</div>';
        else
            $new_messages = '';
        $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum`, `group_real` FROM `Users` WHERE `id`=$_SESSION[id]"));
        if ($result_group_forum[group_real] == 2)
            $result_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `chief` FROM `Students` WHERE `id`=$_SESSION[id]"));     
        echo "
        <div class='lk_menu_block'>
        <ul class='lk_menu_list'>";
            if ($page == "my_page"){
                echo "
                <a href='/forum.php?id=lk'><div class='lk_menu_button_selected'><li>Моя страница</li></div></a>
                <a href='/forum.php?id=lk&lk_page=edit'><li class='lk_menu_list_button'>Редактировать личные данные</li></a>
                <a href='/forum.php?id=lk&lk_page=messages'><li class='lk_menu_list_button'>Мои сообщения $new_messages</li></a>
                <a href='/forum.php?id=lk&lk_page=users_list'><li class='lk_menu_list_button'>Списки пользователей</li></a>";
                if ($result_student_info[chief] == 1)
                    echo "<a href='/forum.php?id=lk&lk_page=chief_page&q=1'><li class='lk_menu_list_button'>Функционал старосты</li></a>";    
                if ($result_group_forum[group_real] == 3)
                    echo "<a href='/forum.php?id=lk&lk_page=show_grades'><li class='lk_menu_list_button'>Просмотр успеваемости</li></a>";   
            }
            if ($page == ""){
                echo "
                <a href='/forum.php?id=lk'><li class='lk_menu_list_button'>Моя страница</li></a>
                <a href='/forum.php?id=lk&lk_page=edit'><li class='lk_menu_list_button'>Редактировать личные данные</li></a>
                <a href='/forum.php?id=lk&lk_page=messages'><li class='lk_menu_list_button'>Мои сообщения $new_messages</li></a>
                <a href='/forum.php?id=lk&lk_page=users_list'><li class='lk_menu_list_button'>Списки пользователей</li></a>"; 
                if ($result_student_info[chief] == 1)
                    echo "<a href='/forum.php?id=lk&lk_page=chief_page&q=1'><li class='lk_menu_list_button'>Функционал старосты</li></a>";
                if ($result_group_forum[group_real] == 3)
                    echo "<a href='/forum.php?id=lk&lk_page=show_grades'><li class='lk_menu_list_button'>Просмотр успеваемости</li></a>"; 
            }
            if ($page == "edit"){
                echo "
                <a href='/forum.php?id=lk'><li class='lk_menu_list_button'>Моя страница</li></a>
                <a href='/forum.php?id=lk&lk_page=edit'><div class='lk_menu_button_selected'><li>Редактировать личные данные</li></div></a>
                <a href='/forum.php?id=lk&lk_page=messages'><li class='lk_menu_list_button'>Мои сообщения $new_messages</li></a>
                <a href='/forum.php?id=lk&lk_page=users_list'><li class='lk_menu_list_button'>Списки пользователей</li></a>";   
                if ($result_student_info[chief] == 1)
                    echo "<a href='/forum.php?id=lk&lk_page=chief_page&q=1'><li class='lk_menu_list_button'>Функционал старосты</li></a>";          
                if ($result_group_forum[group_real] == 3)
                    echo "<a href='/forum.php?id=lk&lk_page=show_grades'><li class='lk_menu_list_button'>Просмотр успеваемости</li></a>";
            }
            if ($page == "messages"){
                echo "
                <a href='/forum.php?id=lk'><li class='lk_menu_list_button'>Моя страница</li></a>
                <a href='/forum.php?id=lk&lk_page=edit'><li class='lk_menu_list_button'>Редактировать личные данные</li></a>
                <a href='/forum.php?id=lk&lk_page=messages'><div class='lk_menu_button_selected'><li>Мои сообщения $new_messages</li></div></a>
                <a href='/forum.php?id=lk&lk_page=users_list'><li class='lk_menu_list_button'>Списки пользователей</li></a>";     
                if ($result_student_info[chief] == 1)
                    echo "<a href='/forum.php?id=lk&lk_page=chief_page&q=1'><li class='lk_menu_list_button'>Функционал старосты</li></a>";        
                if ($result_group_forum[group_real] == 3)
                    echo "<a href='/forum.php?id=lk&lk_page=show_grades'><li class='lk_menu_list_button'>Просмотр успеваемости</li></a>";
            }  
            if ($page == "users_list"){
                echo "
                <a href='/forum.php?id=lk'><li class='lk_menu_list_button'>Моя страница</li></a>
                <a href='/forum.php?id=lk&lk_page=edit'><li class='lk_menu_list_button'>Редактировать личные данные</li></a>
                <a href='/forum.php?id=lk&lk_page=messages'><li class='lk_menu_list_button'>Мои сообщения $new_messages</li></a>
                <a href='/forum.php?id=lk&lk_page=users_list'><div class='lk_menu_button_selected'><li>Списки пользователей</li></div></a>"; 
                if ($result_student_info[chief] == 1)
                    echo "<a href='/forum.php?id=lk&lk_page=chief_page&q=1'><li class='lk_menu_list_button'>Функционал старосты</li></a>";
                if ($result_group_forum[group_real] == 3)
                    echo "<a href='/forum.php?id=lk&lk_page=show_grades'><li class='lk_menu_list_button'>Просмотр успеваемости</li></a>";
            }
            if ($page == "chief_page"){
                echo "
                <a href='/forum.php?id=lk'><li class='lk_menu_list_button'>Моя страница</li></a>
                <a href='/forum.php?id=lk&lk_page=edit'><li class='lk_menu_list_button'>Редактировать личные данные</li></a>
                <a href='/forum.php?id=lk&lk_page=messages'><li class='lk_menu_list_button'>Мои сообщения $new_messages</li></a>
                <a href='/forum.php?id=lk&lk_page=users_list'><li class='lk_menu_list_button'>Списки пользователей</li></a>";          
                if ($result_student_info[chief] == 1)
                    echo "<a href='/forum.php?id=lk&lk_page=chief_page&q=1'><div class='lk_menu_button_selected'><li>Функционал старосты</li></div></a>";
            }
            if ($page == "show_grades"){
                echo "
                <a href='/forum.php?id=lk'><li class='lk_menu_list_button'>Моя страница</li></a>
                <a href='/forum.php?id=lk&lk_page=edit'><li class='lk_menu_list_button'>Редактировать личные данные</li></a>
                <a href='/forum.php?id=lk&lk_page=messages'><li class='lk_menu_list_button'>Мои сообщения $new_messages</li></a>
                <a href='/forum.php?id=lk&lk_page=users_list'><li class='lk_menu_list_button'>Списки пользователей</li></a>"; 
                if ($result_student_info[chief] == 1)
                    echo "<a href='/forum.php?id=lk&lk_page=chief_page&q=1'><li class='lk_menu_list_button'>Функционал старосты</li></a>";
                if ($result_group_forum[group_real] == 3)
                    echo "<a href='/forum.php?id=lk&lk_page=show_grades'><div class='lk_menu_button_selected'><li>Просмотр успеваемости</li></div></a>";
            }
        echo "</ul>
        </div>
        ";
        
    }
    
    //считываем файл excel с 1 листом
    function parse_excel_file( $filename ){
	// подключаем библиотеку
	require_once "Classes/PHPExcel.php";

	$result = array();
        
        $objPHPExcel = PHPExcel_IOFactory::load($filename);
        $result = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        
	return $result;
    }
    
    //считываем файл excel с несколькими листами
    function parse_excel_file_many( $filename ){
	// подключаем библиотеку
	require_once "Classes/PHPExcel.php";

	$result = array();
        
        $objPHPExcel = PHPExcel_IOFactory::load($filename);

        foreach ($objPHPExcel->getWorksheetIterator() as $index => $worksheet) {
            $result[$index] = $worksheet->toArray(null,true,true,true);
        }
        
	return $result;
    }
    
    
    //замена символов из экселя на слова
    function check_lesson($excel_arr, $i, $letter){
        if ($excel_arr[$i][$letter] == '*')
            return 1;
        else
            return 0;    
    }
    
    function insert_day($excel_arr, $table_name, $number_week, $name, $student_id, $i, $letter1, $letter2, $letter3, $letter4){
        global $link;
        
        $date = $excel_arr[2][$letter1];
        $date = date_create($date);
        $date = date_format($date, "Y-F-d");
        
        if ($excel_arr[5][$letter1] == NULL && $excel_arr[5][$letter2] == NULL && $excel_arr[5][$letter3] == NULL && $excel_arr[5][$letter4] == NULL){
            $sql = "INSERT INTO `$table_name` VALUES ('$number_week', '$date', '$name', $student_id, 'Выходной', '2', 'Выходной', '2', 'Выходной', '2', 'Выходной', '2')";
            $query = mysqli_query($link, $sql);         
        }
        else{
            $lesson_1 = $excel_arr[5][$letter1];
            if ($lesson_1 != NULL)
                $is_1 = check_lesson($excel_arr, $i, $letter1);
            else
                $is_1 = 2;

            $lesson_2 = $excel_arr[5][$letter2];
            if ($lesson_2 != NULL)
                $is_2 = check_lesson($excel_arr, $i, $letter2);
            else
                $is_2 = 2;

            $lesson_3 = $excel_arr[5][$letter3];
            if ($lesson_3 != NULL)
                $is_3 = check_lesson($excel_arr, $i, $letter3);
            else
                $is_3 = 2;

            $lesson_4 = $excel_arr[5][$letter4];
            if ($lesson_4 != NULL)
                $is_4 = check_lesson($excel_arr, $i, $letter4);
            else
                $is_4 = 2;

            $sql = "INSERT INTO `$table_name` VALUES ('$number_week', '$date', '$name', $student_id, '$lesson_1', '$is_1', '$lesson_2', '$is_2', '$lesson_3', '$is_3', '$lesson_4', '$is_4')";
            $query = mysqli_query($link, $sql);
        }
    }
    
        function insert_subject($excel_arr, $table_name, $student_name, $student_id, $date, $k, $progress, $hours_miss){
        global $link;
        
        //поиск и связь преподавателя по базе 
        $teacher_name = $excel_arr[1][$k+1][C];
        $subject = $excel_arr[1][$k+1][B];

        preg_match_all("/^[а-яА-Я]+/u", $teacher_name, $mas_surname);  //получаем фамилию из столбца ФИО 
        $surname = $mas_surname[0][0]; 

        preg_match_all("/\s+[а-яА-Я]{0,1}/u", $teacher_name, $mas_name_inicial);//получаем 1 букву имени из столбца ФИО 
        $name_inicial = substr($mas_name_inicial[0][0], 1);

        $pattern1 = $surname." ".$name_inicial;

        $sql = "SELECT Users.id FROM `Users` JOIN `Teachers` ON Users.id = Teachers.id_user JOIN `Subjects` ON Teachers.subject = Subjects.id WHERE Subjects.name = '$subject' AND (Users.name LIKE '%$name%' OR Users.name LIKE '%$pattern1%')";
        $query = mysqli_query($link, $sql);
        $result = mysqli_fetch_assoc($query);

        if ($result[id] != NULL)
            $teacher_id = $result[id];
        else  
            $teacher_id = 0;
        

        $sql = "INSERT INTO `$table_name` VALUES ('$date', '$student_name', $student_id, '$teacher_name', '$teacher_id', '$subject', '$progress', '$hours_miss')";
        $query = mysqli_query($link, $sql);
    }
    
    function readMessages($partner_id){
        global $link;

        $result_dialog_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `id`, `send`, `receive` FROM `Dialogs` WHERE (`send` = '$_SESSION[id]' AND `receive` = '$partner_id') OR (`send` = '$partner_id' AND `receive` = '$_SESSION[id]')"));

        if ($result_dialog_info != NULL && $result_dialog_info[receive] == $_SESSION[id]){
            $sql = "UPDATE `Dialogs` SET `status` = 0 WHERE `send` = '$partner_id' AND `receive` = '$_SESSION[id]'";
            $query = mysqli_query($link, $sql);

            $sql = "UPDATE `Personal_messages` SET `status` = 0 WHERE `id_dialog` = '$result_dialog_info[id]' AND `id_author` = $partner_id AND `status` = 1";
            $query = mysqli_query($link, $sql);
        }
        
    }
    
    function email(){
        newMessage(1, 'E-mail адрес <b>'.$_SESSION['user_active_email'].'</b> уже подтвержден.', '/forum.php');
    }
    
    function check_online(){
        global $link;
        
        if ($_SESSION[vhod]){
            $date = date("Y-m-d H-i-s");
            $sql = "UPDATE `Settings` SET `last_visit` = '$date' WHERE `user_id` = '$_SESSION[id]'";
            $query = mysqli_query($link, $sql);
        }        
    }
    
    function show_online($id_user) {
        global $link;
        
        $result_online_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `last_visit` FROM `Settings` WHERE `user_id` = '$id_user'"));             
        $last_visit = strtotime($result_online_info[last_visit]);//переводим строку в секунды
        $fifteen_min_ago = time() - 900;
        if ($last_visit < $fifteen_min_ago){
            $string = '<b>Последний вход : </b>'.checkMyDate($result_online_info[last_visit]);
            return $string;
        }
        else
            return '<b>Online</b>';
    }
    

     
     
    