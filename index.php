<?php
    session_start(); 
    include_once "inc/config.inc.php";
    include "inc/lib.inc.php";    
    include "inc/handler.php";
    check_online();
?>
<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Главная страница</title>
	<link rel="stylesheet" type="text/css" href="resource/style.css">
        <!--<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>-->
        <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
        <script src="/resource/scripts/jquery.min.js" type="text/javascript"></script>
        <script src="/resource/scripts/scripts.js"></script>
</head>
<body>
	<div class="wrapper">	
             <div class="header">
                 <div class="info">
                     <?php
                        if ($_SESSION["vhod"])
                            echo $_SESSION[name].",
                            <form class='info_form' action=".$_SERVER[REQUEST_URI]." method='POST'>
                                <input class='info_form_button' name='exit_submit' type='submit' value='Выход'>
                            </form>";
                        else
                            echo "Вы не авторизованы."
                     ?>
                 </div>
                <div class="logo">
                    <img src="resource/images/logo2.png" alt="Лого" width="600px">
                </div>
                <nav class="menu">
                    <ul class="list">
                        <a href="/"><li class="menu-button">Новости</li></a>
                        
                        <a href="/forum.php"><li class="menu-button">Форум</li></a>
                    </ul>
                </nav>
            </div>
            <?php 
            showMessage();?>
	    <div class="content">
                <?php  
                    getNews();
                ?>              
	    </div>
            <?php
               include_once "inc/footer.inc.php";
            ?>	   
	</div>
	
</body>
</html>