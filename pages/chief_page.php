<?php
    if ($_SESSION["vhod"]){
        $result = mysqli_fetch_assoc(mysqli_query($link, "SELECT Users.group_forum, Users.group_real, Students.chief FROM `Users` JOIN `Students` ON Users.id = Students.id_user WHERE Users.id=$_SESSION[id]"));
        getLkMenu("chief_page");
        if ($result[group_forum] == 5){//если пользователь заблокирован
            echo "<div class='infoblock infoblock_lk infoblock_lk_edit'>Вы не можете просматривать данную страницу. Ваш аккаунт заблокирован!<br> Свяжитесь с администратором.</div>";
        }
        else if ($result[chief] == 0)//если пользователь не староста
            echo "<div class='infoblock infoblock_lk infoblock_lk_edit'>У вас нет прав доступа для просмотра данной страницы, т.к. вы не являетесь старостой.</div>";
        else{
            if ($q == 1 || $q == NULL){//страница загрузки
                echo "
                <div class='chief_page_menu'>
                    <a href='forum.php?id=lk&lk_page=chief_page&q=1'><div class='chief_page_menu_button'>Страница загрузки</div></a>
                    <a href='forum.php?id=lk&lk_page=chief_page&q=2'><div class='chief_page_menu_button chief_page_menu_button2'>Страница просмотра
                        <div class='chief_page_menu_hidden' onclick=\"window.location.href='forum.php?id=lk&lk_page=chief_page&q=2&f=1'; return false\">Журналы</div>
                        <div class='chief_page_menu_hidden chief_page_menu_hidden2' onclick=\"window.location.href='forum.php?id=lk&lk_page=chief_page&q=2&f=2'; return false\">Аттестации</div>
                    </div></a>
                </div>
                <div class='lk_main'>
                    <p id='chief_page_info'>
                        Примеры правильного заполнения журналов и аттестаций:<br /><br />
                        1)<a href='../resource/templates/journal_template.xlsx'>Электронный журнал</a><br/ >
                        2)<a href='../resource/templates/attestation_template.xlsx'>Электронная аттестация</a> (На втором листе находится список дисциплин, будьте внимательны, не пропустите его!)<br/ >
                    </p>
                    <form class='chief_page_form' action='$_SERVER[REQUEST_URI]' method='POST' enctype='multipart/form-data'>
                        <h2>Загрузка журнала</h2>
                        <label>Введите номер группы<br \><input class='chief_page_form_field' type='number' name='number_group' min='1' placeholder='Номер группы' required></label><br /><br />
                        <label>Введите номер недели журнала<br \><input class='chief_page_form_field' type='number' name='number_week' min='1' placeholder='Номер недели' required></label><br /><br />
                        <label>Прикрепить файл журнала<br \><input type='file' name='journal_file'></label><br /><br />
                        <input class='button' name='submit_save_journal_file' type='submit' value='Загрузить'>
                    </form>
                    <form class='chief_page_form' action='$_SERVER[REQUEST_URI]' method='POST' enctype='multipart/form-data'>
                        <h2>Загрузка аттестации</h2>
                        <label>Введите номер группы<br \><input class='chief_page_form_field' type='number' name='number_group_at' min='1' placeholder='Номер группы' required></label><br /><br />
                        <label>Прикрепить файл аттестации<br \><input type='file' name='attestation_file'></label><br /><br />
                        <input class='button' name='submit_save_attestation_file' type='submit' value='Загрузить'>
                    </form>
                </div>
                ";
            }
            else if ($q == 2){//страница просмотра журналов
                if ($f == 1 || $f == NULL){
                    $sql = "SHOW TABLES LIKE 'Journal%'";
                    $query = mysqli_query($link, $sql);
                    $result = mysqli_fetch_all($query);
                    echo "
                    <div class='chief_page_menu'>
                        <a href='forum.php?id=lk&lk_page=chief_page&q=1'><div class='chief_page_menu_button'>Страница загрузки</div></a>
                        <a href='forum.php?id=lk&lk_page=chief_page&q=2'><div class='chief_page_menu_button chief_page_menu_button2'>Страница просмотра
                            <div class='chief_page_menu_hidden' onclick=\"window.location.href='forum.php?id=lk&lk_page=chief_page&q=2&f=1'; return false\">Журналы</div>
                            <div class='chief_page_menu_hidden chief_page_menu_hidden2' onclick=\"window.location.href='forum.php?id=lk&lk_page=chief_page&q=2&f=2'; return false\">Аттестации</div>
                        </div></a>
                    </div>
                    <div class='lk_main'>";
                        if ($result != NULL){//если найдены журналы в базе
                             echo"
                            <p id='chief_page_info'>
                                Информация:<br />
                                1)Если нажать 'Показать' без указания недели, журнал выбранной группы будет показан полностью! <br/ >
                                2)Если нажать 'Удалить журнал' без указания недели, журнал выбранной группы будет удален полностью!<br/ >
                                3)Если в поле 'Студент' присутствует ссылка на аккаунт, это говорит об успешной связи загруженных данных с аккаунтом студента. Если ссылки нет, возможно, вы перепутали номер группы или данный студент здесь не зарегистрирован.<br/ >
                            </p>";
                            echo "
                            <form class='lk_edit_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <div class='chief_page_form_part'>
                                    <label>Загруженные журналы<br />
                                    <select class='chief_page_form_field' size='1' name='journal_name'>";
                                        for ($i = 0; $i < count($result); $i++){
                                            $name = $result[$i][0];
                                            if ($name == $_SESSION[journal_name]){
                                                $selected = 'selected';
                                            }
                                            echo "<option $selected value='$name'>$name</option>";
                                            $selected = '';
                                        }
                                        echo"
                                    </select></label>
                                </div>
                                <div class='chief_page_form_part'>
                                    <label>Выбрать неделю<br /><input class='chief_page_form_field' type='number' name='journal_week' min='1' placeholder='Неделя' value='$_SESSION[journal_week]'></label> 
                                </div>
                                <div class='chief_page_form_button'>
                                    <input class='button' name='submit_chief_page_show_journals' type='submit' value='Показать'>  
                                </div>
                                <div class='chief_page_form_button'>
                                    <input class='button' name='submit_chief_page_delete_journal' type='submit' value='Удалить журнал'>  
                                </div>
                            </form>"; 
                            if ($_SESSION[journal_name]){
                                if ($_SESSION[journal_week] != 0)
                                    $result_journal = mysqli_fetch_all(mysqli_query($link, "SELECT `week`, `date`, `name`, `lesson_1`, `is_1`, `lesson_2`, `is_2`, `lesson_3`, `is_3`, `lesson_4`, `is_4`, `student_id` FROM `$_SESSION[journal_name]` WHERE `week` = '$_SESSION[journal_week]'"));
                                else
                                    $result_journal = mysqli_fetch_all(mysqli_query($link, "SELECT `week`, `date`, `name`, `lesson_1`, `is_1`, `lesson_2`, `is_2`, `lesson_3`, `is_3`, `lesson_4`, `is_4`, `student_id` FROM `$_SESSION[journal_name]`"));

                                if ($result_journal == NULL)
                                    echo "<p> Журнал с указанной неделей не найден!</p>";
                                else{
                                    echo "
                                    <table class='journal_table'>
                                        <tr>
                                            <th>Неделя</th>
                                            <th>Дата</th>
                                            <th>Студент</th>
                                            <th>1 пара</th>
                                            <th>Был</th>
                                            <th>2 пара</th>
                                            <th>Был</th>
                                            <th>3 пара</th>
                                            <th>Был</th>
                                            <th>4 пара</th>
                                            <th>Был</th>
                                        </tr>
                                        <col width='5%'>
                                        <col width='auto'>
                                        <col width='auto'>
                                        <col width='auto'>
                                        <col width='5%'>
                                        <col width='auto'>
                                        <col width='5%'>
                                        <col width='auto'>
                                        <col width='5%'>
                                        <col width='auto'>
                                        <col width='5%'>
                                        ";
                                    for ($i = 0; $i < count($result_journal); $i++){
                                        $week = $result_journal[$i][0];
                                        $date = $result_journal[$i][1];
                                        $name = $result_journal[$i][2];

                                        if ($result_journal[$i][4] == 1){
                                            $lesson_1 = ">".$result_journal[$i][3];
                                            $is_1 = '>Да';
                                        }
                                        else{
                                            $lesson_1 = " style=\"color: red;\">".$result_journal[$i][3];
                                            if ($result_journal[$i][4] == 0)
                                                $is_1 = ' style=\'color: red;\'>Нет';
                                            else 
                                                $is_1 = '>';
                                        }

                                        if ($result_journal[$i][6] == 1){
                                            $lesson_2 = ">".$result_journal[$i][5];
                                            $is_2 = '>Да';
                                        }
                                        else{
                                            $lesson_2 = " style=\"color: red;\">".$result_journal[$i][5];
                                            if ($result_journal[$i][6] == 0)
                                                $is_2 = ' style=\'color: red;\'>Нет';
                                            else 
                                                $is_2 = '>';
                                        }

                                        if ($result_journal[$i][8] == 1){
                                            $lesson_3 = ">".$result_journal[$i][7];
                                            $is_3 = '>Да';
                                        }
                                        else{
                                            $lesson_3 = " style=\"color: red;\">".$result_journal[$i][7];
                                            if ($result_journal[$i][8] == 0)
                                                $is_3 = ' style=\'color: red;\'>Нет';
                                            else 
                                                $is_3 = '>';
                                        }

                                        if ($result_journal[$i][10] == 1){
                                            $lesson_4 = ">".$result_journal[$i][9];
                                            $is_4 = '>Да';
                                        }
                                        else{
                                            $lesson_4 = " style=\"color: red;\">".$result_journal[$i][9];
                                            if ($result_journal[$i][10] == 0)
                                                $is_4 = ' style=\'color: red;\'>Нет';
                                            else 
                                                $is_4 = '>';
                                        }

                                        if ($result_journal[$i][11] != 0)
                                            $student_id = $result_journal[$i][11];
                                        echo "
                                        <tr>
                                            <td>$week</td>
                                            <td>$date</td>";
                                            if ($student_id != 0)
                                                echo "<td><a href='forum.php?id=lk&user=$student_id'>$name</a></td>";
                                            else
                                                echo "<td>$name</td>";
                                            $student_id = 0;
                                            echo"
                                            <td$lesson_1</td>
                                            <td$is_1</td>
                                            <td$lesson_2</td>
                                            <td$is_2</td>
                                            <td$lesson_3</td>
                                            <td$is_3</td>
                                            <td$lesson_4</td>
                                            <td$is_4</td>
                                        </tr>";
                                    }            
                                    echo "</table>";
                                }
                            }
                        }
                        else//если журналы в базе не найдены
                            echo "В базе нет загруженных журналов!";
                    echo " 
                    </div>
                    ";
                }
                else if ($f == 2){
                    $sql = "SHOW TABLES LIKE 'Attestation%'";
                    $query = mysqli_query($link, $sql);
                    $result_at = mysqli_fetch_all($query);
                    echo "
                    <div class='chief_page_menu'>
                        <a href='forum.php?id=lk&lk_page=chief_page&q=1'><div class='chief_page_menu_button'>Страница загрузки</div></a>
                        <a href='forum.php?id=lk&lk_page=chief_page&q=2'><div class='chief_page_menu_button chief_page_menu_button2'>Страница просмотра
                            <div class='chief_page_menu_hidden' onclick=\"window.location.href='forum.php?id=lk&lk_page=chief_page&q=2&f=1'; return false\">Журналы</div>
                            <div class='chief_page_menu_hidden chief_page_menu_hidden2' onclick=\"window.location.href='forum.php?id=lk&lk_page=chief_page&q=2&f=2'; return false\">Аттестации</div>
                        </div></a>
                    </div>
                    <div class='lk_main'>";
                        if ($result_at != NULL){//если найдены аттестации
                            echo"
                            <p id='chief_page_info'>
                                Информация:<br />
                                1)Если в поле 'Студент' и 'Преподаватель' присутствуют ссылки на аккаунт, это говорит об успешной связи загруженных данных с аккаунтами студентов и преподавателей.<br/ >
                            </p>";
                            echo"
                            <form class='lk_edit_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <div class='chief_page_form_part'>
                                    <label>Загруженные аттестации<br />
                                    <select class='chief_page_form_field' size='1' name='attestation_name'>";
                                        for ($i = 0; $i < count($result_at); $i++){
                                            $name = $result_at[$i][0];
                                            if ($name == $_SESSION[attestation_name]){
                                                $selected = 'selected';
                                            }
                                            echo "<option $selected value='$name'>$name</option>";
                                            $selected = '';
                                        }
                                        echo"
                                    </select></label>";
                                    if ($_SESSION[attestation_name]){
                                        $sql = "SELECT DISTINCT `date` FROM `$_SESSION[attestation_name]`";
                                        $query = mysqli_query($link, $sql);
                                        $result_date = mysqli_fetch_all($query);
                                        echo"
                                        <select class='chief_page_form_field' size='1' name='attestation_date'>";
                                            for ($i = 0; $i < count($result_date); $i++){
                                                $date = $result_date[$i][0];
                                                if ($date == $_SESSION[attestation_date]){
                                                    $selected = 'selected';
                                                }
                                                echo "<option $selected value='$date'>$date</option>";
                                                $selected = '';
                                            }
                                            echo"
                                        </select></label>";
                                    }
                                    
                                echo"
                                </div>
                                <div class='chief_page_form_button'>
                                    <input class='button' name='submit_chief_page_show_attestation' type='submit' value='Показать'>  
                                </div>
                                <div class='chief_page_form_button'>
                                    <input class='button' name='submit_chief_page_delete_attestation' type='submit' value='Удалить аттестацию'>  
                                </div>
                            </form>";
                                        
                            if ($_SESSION[attestation_name]){
                                $result_attestation = mysqli_fetch_all(mysqli_query($link, "SELECT `date`, `student_name`, `student_id`, `teacher_name`, `teacher_id`, `lesson_name`, `progress`, `hours_miss` FROM `$_SESSION[attestation_name]`"));

                                if ($result_attestation == NULL)
                                    echo "<p>Аттестация не найдена!</p>";
                                else{
                                    echo "
                                    <table class='journal_table'>
                                        <tr>
                                            <th>Дата</th>
                                            <th>Студент</th>
                                            <th>Преподаватель</th>
                                            <th>Предмет</th>
                                            <th>Успеваемость</th>
                                            <th>Часов пропущено</th>
                                        </tr>";
                                    for ($i = 0; $i < count($result_attestation); $i++){
                                        $date = $result_attestation[$i][0];
                                        $student_name = $result_attestation[$i][1];
                                        if ($result_attestation[$i][2] != 0)
                                            $student_id = $result_attestation[$i][2];
                                        $teacher_name = $result_attestation[$i][3];
                                        if ($result_attestation[$i][4] != 0)
                                            $teacher_id = $result_attestation[$i][4];
                                        $lesson_name = $result_attestation[$i][5];

                                        if ($result_attestation[$i][6] != 0){
                                            $progress = ">".$result_attestation[$i][6];
                                        }
                                        else{
                                            $progress = " style=\"color: red;\">".$result_attestation[$i][6];
                                        }

                                        if ($result_attestation[$i][7] == 0){
                                            $hours_miss = ">".$result_attestation[$i][7];
                                        }
                                        else{
                                            $hours_miss = " style=\"color: red;\">".$result_attestation[$i][7];
                                        }

                                        echo "
                                        <tr>
                                            <td>$date</td>";
                                            if ($student_id != 0)
                                                echo "<td><a href='forum.php?id=lk&user=$student_id'>$student_name</a></td>";
                                            else
                                                echo "<td>$student_name</td>";
                                            $student_id = 0;
                                            if ($teacher_id != 0)
                                                echo "<td><a href='forum.php?id=lk&user=$teacher_id'>$teacher_name</a></td>";
                                            else
                                                echo "<td>$teacher_name</td>";
                                            $teacher_id = 0;
                                            echo"
                                            <td>$lesson_name</td>
                                            <td$progress</td>
                                            <td$hours_miss</td>
                                        </tr>";
                                    }            
                                    echo "</table>";
                                }
                            }                 
                        }
                        else
                            echo "В базе нет загруженных аттестаций!";
                    echo"
                    </div>";
                }
            }
        }
    }
    else
        echo "<div class='infoblock infoblock_lk'>Пожалуйста, авторизуйтесь!</div>"; 
?>