<?php
    if (($_SESSION[vhod]) && (($user == $_SESSION[id]) || ($user == NULL) )) {  //Вход на свою страницу
        getLkMenu("my_page");
        if ($user == NULL) 
            $user = $_SESSION[id];
        $sql = "SELECT `id`, `name`, `avatar_folder`, `registration_date`, `email`, `phone_number`, `age`, `messages`, `topics`, `group_forum`, `group_real` FROM `Users` WHERE `id`='$user'";
        $query = mysqli_query($link, $sql);
        $result = mysqli_fetch_assoc ($query);
        $result_group_real = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Group_real` WHERE `id`=$result[group_real]"));
        $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Group_forum` WHERE `id`=$result[group_forum]")); 
        $result_settings = mysqli_fetch_assoc(mysqli_query($link, "SELECT `show_email`, `show_tel` FROM `Settings` WHERE `user_id` = $_SESSION[id]")); 
        if ($result_settings[show_email] == 1)
            $show_email = '(Скрыт)';
        if ($result_settings[show_tel] == 1)
            $show_tel = '(Скрыт)';
        
        if ($result[group_real] == 2)//если пользователь студент
            $result_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `course`, `group`, `chief` FROM `Students` WHERE `id_user`='$_SESSION[id]'"));           
        else if ($result[group_real] == 3)//если пользователь родитель студента
            $result_parent_info = mysqli_fetch_all(mysqli_query($link, "SELECT `link_student` FROM `Parents` WHERE `id_user`='$_SESSION[id]'"));             
         else if ($result[group_real] == 1 || $result[group_real] == 4)//если пользователь преподаватель или зав.кафедры
            $result_teacher_info = mysqli_fetch_all(mysqli_query($link, "SELECT Teachers.subject, Subjects.name FROM `Teachers` JOIN `Subjects` ON Teachers.subject = Subjects.id WHERE Teachers.id_user = '$_SESSION[id]'"));             
 
        echo "
        <div class='lk_main'>
            <div class='online_block'>"; 
                echo show_online($_SESSION[id]); 
            echo "
            </div>
            <div class='lk_left_side_block'>";
                if ($result[avatar_folder] != 0)//если нету аватара
                    echo "<div class='lk_photo' style='background-image: url(resource/avatar/$result[avatar_folder]/$_SESSION[id].jpg); background-size: cover;'></div>";
                else
                    echo "<div class='lk_photo' style='background: url(resource/avatar/all.png); background-size: cover; background-position: center;'></div>";
                echo "     
                    <div class='lk_buttons_block'>                       
                        <div class='lk_default_buttons_block'>
                            <a href='forum.php?id=lk&lk_page=edit' class='button lk_buttons' style='font-size: 13px;'>
                                Сменить фотографию
                            </a>
                        </div>";
                        if ($result[group_forum] == 2){//если пользователь - админ
                            echo "
                            <form class='lk_buttons_block' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <input type='hidden' name='id_user' value='$_SESSION[id]'>
                                <div class='lk_admin_buttons_block'>
                                    <div class='lk_admin_panel_text'>Панель администратора</div>";//панель администратора
                                    if ($result[group_real] == 3)
                                        echo "<input type='button' id='create_relation_button' class='button lk_buttons' name='' value='Привязка к студентам'>";
                                    else if ($result[group_real] == 2){
                                       if ($result_student_info[chief] != 1)
                                            echo "<input type='submit' class='button lk_buttons' name='submit_appoint_chief' value='Назначить себя старостой'>";
                                        else
                                            echo "<input type='submit' class='button lk_buttons' name='submit_disappoint_chief' value='Снять себя с должности старосты'>";
                                    }
                                    else if ($result[group_real] == 1 || $result[group_real] == 4){
                                        echo "<input type='button' id='create_subject_button' class='button lk_buttons' name='' value='Мои предметы'>";
                                    }
                                    echo "<input type='button' id='add_subject_button' class='button lk_buttons' name='' value='Предметы в базе данных'>";
                                    
                                echo "
                                </div>
                            </form>";
                            echo "<div id='overlay'></div>";  
                            if ($result[group_real] == 3){//модуль привязки родителя к студенту
                                $result_student_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.name, Students.group, Users.id FROM `Users` JOIN `Students` ON Users.id = Students.id_user ORDER BY `group` DESC"));
                                $count_students = count($result_student_list);
                                $count_link_students = count($result_parent_info);
                                echo "
                                <div id='create_relation_block'>
                                    <div id='button_close'></div>
                                    <br /><span style='margin-left: 20px;'>$result[name]</span><br /><br />
                                    <form class='create_relation_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                        <input type='hidden' name='parent_id' value='$result[id]'>
                                        <label>Привязаные студенты:<br />
                                        <select class='create_relation_now' size='$count_link_students' name='delete_relation_student' required>";

                                            for ($i = 0; $i < $count_link_students; $i++){
                                                $id_student = $result_parent_info[$i][0];
                                                $result_link_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT Users.name, Students.group FROM `Users` JOIN `Students` ON Users.id = Students.id_user WHERE Users.id = '$id_student'"));
                                                $name_student = $result_link_student_info['name'];
                                                $group_student = $result_link_student_info['group'];

                                                echo "<option value='$id_student'>Группа:$group_student / $name_student</option>";
                                            }
                                            echo"
                                        </select></label><br /><br />
                                       <input type='submit' class='button topic_buttons' name='submit_delete_relation' value='Удалить связь'>
                                    </form>
                                    <hr class='my_hr' />
                                    <form class='create_relation_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                        <input type='hidden' name='parent_id' value='$result[id]'>
                                        <label>Связать с:<br />
                                        <select class='create_relation_select' size='$count_students' name='relation_student' required>";
                                            for ($i = 0; $i < $count_students; $i++){
                                                $name_student = $result_student_list[$i][0];
                                                $group_student = $result_student_list[$i][1];
                                                $id_student = $result_student_list[$i][2];


                                                if (in_array($id_student, $result_parent_info))
                                                    continue;
                                                else
                                                    echo "<option value='$id_student'>Группа:$group_student / $name_student</option>";
                                            }
                                            echo"
                                        </select></label><br /><br />
                                       <input type='submit' class='button topic_buttons' name='submit_create_relation' value='Связать'>
                                    </form>
                                </div>";
                            }  
                                   
                            if ($result[group_real] == 1 || $result[group_real] == 4){//модуль добавления предметов преподавателю
                                $result_subjects_list = mysqli_fetch_all(mysqli_query($link, "SELECT `id`, `name` FROM `Subjects` ORDER BY `name`"));
                                $count_subjects = count($result_subjects_list);
                                $count_teacher_subjects = count($result_teacher_info);
                                echo "
                                <div id='create_relation_block'>
                                    <div id='button_close'></div>
                                    <br /><span style='margin-left: 20px;'>$result[name]</span><br /><br />
                                    <form class='create_relation_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                        <input type='hidden' name='teacher_id' value='$result[id]'>
                                        <label>Уже ведет предметы:<br />
                                        <select class='create_relation_now' size='$count_teacher_subjects' name='delete_id_subject' required>";

                                            for ($i = 0; $i < $count_teacher_subjects; $i++){
                                                $id_subject = $result_teacher_info[$i][0];
                                                $name_subject = $result_teacher_info[$i][1];

                                                echo "<option value='$id_subject'>$name_subject</option>";
                                            }
                                            echo"
                                        </select></label><br /><br />
                                       <input type='submit' class='button topic_buttons' name='submit_delete_subject' value='Удалить предмет'>
                                    </form>
                                    <hr class='my_hr' />
                                    <form class='create_relation_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                        <input type='hidden' name='teacher_id' value='$result[id]'>
                                        <label>Добавить предмет:<br />
                                        <select class='create_relation_select' size='$count_subjects' name='add_id_subject' required>";
                                            for ($i = 0; $i < $count_subjects; $i++){
                                                $name_subject = $result_subjects_list[$i][1];
                                                $id_subject = $result_subjects_list[$i][0];


                                                if (in_array($id_subject, $result_teacher_info))
                                                    continue;
                                                else
                                                    echo "<option value='$id_subject'>$name_subject</option>";
                                            }
                                            echo"
                                        </select></label><br /><br />
                                       <input type='submit' class='button topic_buttons' name='submit_add_subject' value='Добавить'>
                                    </form>
                                </div>";
                            }
                            
                            //модуль добавления предметов в бд
                            $result_subjects_list = mysqli_fetch_all(mysqli_query($link, "SELECT `id`, `name` FROM `Subjects` ORDER BY `name`"));
                            $count_subjects = count($result_subjects_list);
                            echo "
                            <div id='add_subject_block'>
                                <div id='button_close1'></div>
                                    <form class='add_subject_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                        <label>Добавить предмет в базу данных:<br /><br />
                                        <input type='text' class='add_subject_name_field' pattern='^[a-zA-Zа-яА-Я0-9\s-]+$' name='add_subject_name' title='Используйте только русские буквы или цифры!' placeholder='Название' required></label><br /><br />
                                       <input type='submit' class='button topic_buttons' name='submit_add_subject_to_db' value='Добавить'><br />
                                    </form>
                                    <hr class='my_hr' />
                                    <form class='add_subject_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                        <input type='hidden' name='teacher_id' value='$result[id]'>
                                        <label>Предметы в базе данных:<br />
                                        <select class='create_relation_select' size='$count_subjects' name='delete_id_subject_from_bd' required>";
                                            for ($i = 0; $i < $count_subjects; $i++){
                                                $name_subject = $result_subjects_list[$i][1];
                                                $id_subject = $result_subjects_list[$i][0];


                                                if (in_array($id_subject, $result_teacher_info))
                                                    continue;
                                                else
                                                    echo "<option value='$id_subject'>$name_subject</option>";
                                            }
                                            echo"
                                        </select></label><br /><br />
                                       <input type='submit' class='button topic_buttons' name='submit_delete_subject_from_db' value='Удалить'>
                                    </form>
                            </div>";  
                        }
                    echo "    
                    </div>            
            </div>
            <div class='lk_info'>
                <b>id:</b> $result[id]<br/>
                <hr class='main_lk_hr'>
                <b>Имя:</b> $result[name]<br/>
                <hr class='main_lk_hr'>
                <b>Дата регистрации:</b> $result[registration_date]<br/>
                <hr class='main_lk_hr'>
                <b>E-mail $show_email:</b> $result[email]<br/>
                <hr class='main_lk_hr'>
                <b>Номер телефона $show_tel:</b> $result[phone_number]<br/>
                <hr class='main_lk_hr'>
                <b>Возраст:</b> $result[age]<br/>
                <hr class='main_lk_hr'>
                <b>Сообщений на форуме:</b> $result[messages]<br/>
                <hr class='main_lk_hr'>
                <b>Созданных тем:</b> $result[topics]<br/>
                <hr class='main_lk_hr'>
                <b>Статус:</b> <span style='color:red'>$result_group_forum[name]</span><br/>
                <hr class='main_lk_hr'>
                <b>Должность:</b> $result_group_real[name]<br/>
                <hr class='main_lk_hr'>";
                if ($result[group_real] == 2){
                    if ($result_student_info[chief] == 1)
                        $chief = "Да";
                    else 
                        $chief = "Нет";
                    echo "
                        <b>Курс:</b> $result_student_info[course]<br/>
                        <hr class='main_lk_hr'>
                        <b>Группа:</b> $result_student_info[group]<br/>
                        <hr class='main_lk_hr'>
                        <b>Староста:</b> $chief<br/>
                        <hr class='main_lk_hr'>
                    ";                                   
                }
                else if ($result[group_real] == 3){
                    if ($result_parent_info[0][0] != 0){
                        echo "
                        <b>Связан со студентом: </b>";
                        for ($i = 0; $i < count($result_parent_info); $i++){
                            $link_student = $result_parent_info[$i][0];
                            $result_link_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Users` WHERE `id` = '$link_student'"));           
                            echo "<a href='forum.php?id=lk&user=$link_student'>$result_link_student_info[name]</a><br/>";
                        }
                        echo "<hr class='main_lk_hr'>";
                    }
                    else 
                        echo "
                        <b>Связан со студентом:</b> Вы не привязаны к студенту! Свяжитесь с администратором<br/>
                        <hr class='main_lk_hr'>";                                  
                }
                else if ($result[group_real] == 1 || $result[group_real] == 4){
                    if ($result_teacher_info[0][0] != 0){
                        echo "
                        <b>Предмет: </b>";
                        for ($i = 0; $i < count($result_teacher_info); $i++){
                            $subject = $result_teacher_info[$i][1];
                            echo "$subject<br/>";
                        }
                        echo "<hr class='main_lk_hr'>";
                    }
                    else 
                        echo "
                        <hr class='main_lk_hr'>
                        <b>Предмет:</b> У вас не выбран предмет, который вы ведете! Свяжитесь с администратором.<br/>
                        <hr class='main_lk_hr'>";                                  
                }
                
            echo "      
            </div>
        </div>";
    }
    else if (($_SESSION[vhod]) && (isset($user))){   //Вход на страницу других пользователей
        getLkMenu("");
        $sql = "SELECT `id`, `name`, `avatar_folder`, `registration_date`, `email`, `phone_number`, `age`, `messages`, `topics`, `group_forum`, `group_real` FROM `Users` WHERE `id`='$user'";
        $query = mysqli_query($link, $sql);
        if (!$query){
            newMessage (1, 'Неверный SQL-запрос! ('.mysqli_error($link).') Обратитесь к администратору.');
        }
        else {
            $result = mysqli_fetch_assoc ($query);
            if ($result == NULL)
                echo "<div class='infoblock infoblock_lk_page'>Данного пользователя нет в наше базе.</div>";
            else{   
                $result_group_real = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Group_real` WHERE `id`=$result[group_real]"));
                $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Group_forum` WHERE `id`=$result[group_forum]"));
                $result_my_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum` FROM `Users` WHERE `id`=$_SESSION[id]"));
                $result_settings = mysqli_fetch_assoc(mysqli_query($link, "SELECT `show_email`, `show_tel` FROM `Settings` WHERE `user_id` = $result[id]")); 
                
                
                if ($result[group_real] == 2) 
                    $result_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `course`, `group`, `chief` FROM `Students` WHERE `id_user`='$result[id]'"));           
                else if ($result[group_real] == 3) 
                    $result_parent_info = mysqli_fetch_all(mysqli_query($link, "SELECT `link_student` FROM `Parents` WHERE `id_user`='$result[id]'"));             
                else if ($result[group_real] == 1 || $result[group_real] == 4)//если пользователь преподаватель или зав.кафедры
                    $result_teacher_info = mysqli_fetch_all(mysqli_query($link, "SELECT Teachers.subject, Subjects.name FROM `Teachers` JOIN `Subjects` ON Teachers.subject = Subjects.id WHERE Teachers.id_user = '$result[id]'"));             
 
                if ($result_my_group_forum[group_forum] == 2){//если вошедший пользователь - админ
                    echo "<div id='overlay'></div>";
                    if ($result[group_real] == 3){//модуль привязки родителя к студенту
                        $result_student_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.name, Students.group, Users.id FROM `Users` JOIN `Students` ON Users.id = Students.id_user ORDER BY `group` DESC"));
                        $count_students = count($result_student_list);
                        $count_link_students = count($result_parent_info);
                        echo "
                        <div id='create_relation_block'>
                            <div id='button_close'></div>
                            <br /><span style='margin-left: 20px;'>$result[name]</span><br /><br />
                            <form class='create_relation_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <input type='hidden' name='parent_id' value='$result[id]'>
                                <label>Привязаные студенты:<br />
                                <select class='create_relation_now' size='$count_link_students' name='delete_relation_student' required>";

                                    for ($i = 0; $i < $count_link_students; $i++){
                                        $id_student = $result_parent_info[$i][0];
                                        $result_link_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT Users.name, Students.group FROM `Users` JOIN `Students` ON Users.id = Students.id_user WHERE Users.id = '$id_student'"));
                                        $name_student = $result_link_student_info['name'];
                                        $group_student = $result_link_student_info['group'];

                                        echo "<option value='$id_student'>Группа:$group_student / $name_student</option>";
                                    }
                                    echo"
                                </select></label><br /><br />
                               <input type='submit' class='button topic_buttons' name='submit_delete_relation' value='Удалить связь'>
                            </form>
                            <hr class='my_hr' />
                            <form class='create_relation_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <input type='hidden' name='parent_id' value='$result[id]'>
                                <label>Связать с:<br />
                                <select class='create_relation_select' size='$count_students' name='relation_student' required>";
                                    for ($i = 0; $i < $count_students; $i++){
                                        $name_student = $result_student_list[$i][0];
                                        $group_student = $result_student_list[$i][1];
                                        $id_student = $result_student_list[$i][2];


                                        if (in_array($id_student, $result_parent_info))
                                            continue;
                                        else
                                            echo "<option value='$id_student'>Группа:$group_student / $name_student</option>";
                                    }
                                    echo"
                                </select></label><br /><br />
                               <input type='submit' class='button topic_buttons' name='submit_create_relation' value='Связать'>
                            </form>
                        </div>";
                    }

                    if ($result[group_real] == 1 || $result[group_real] == 4){//модуль добавления предметов
                        $result_subjects_list = mysqli_fetch_all(mysqli_query($link, "SELECT `id`, `name` FROM `Subjects` ORDER BY `name`"));
                        $count_subjects = count($result_subjects_list);
                        $count_teacher_subjects = count($result_teacher_info);
                        echo "
                        <div id='create_relation_block'>
                            <div id='button_close'></div>
                            <br /><span style='margin-left: 20px;'>$result[name]</span><br /><br />
                            <form class='create_relation_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <input type='hidden' name='teacher_id' value='$result[id]'>
                                <label>Уже ведет предметы:<br />
                                <select class='create_relation_now' size='$count_teacher_subjects' name='delete_id_subject' required>";

                                    for ($i = 0; $i < $count_teacher_subjects; $i++){
                                        $id_subject = $result_teacher_info[$i][0];
                                        $name_subject = $result_teacher_info[$i][1];

                                        echo "<option value='$id_subject'>$name_subject</option>";
                                    }
                                    echo"
                                </select></label><br /><br />
                               <input type='submit' class='button topic_buttons' name='submit_delete_subject' value='Удалить предмет'>
                            </form>
                            <hr class='my_hr' />
                            <form class='create_relation_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <input type='hidden' name='teacher_id' value='$result[id]'>
                                <label>Добавить предмет:<br />
                                <select class='create_relation_select' size='$count_subjects' name='add_id_subject' required>";
                                    for ($i = 0; $i < $count_subjects; $i++){
                                        $name_subject = $result_subjects_list[$i][1];
                                        $id_subject = $result_subjects_list[$i][0];


                                        if (in_array($id_subject, $result_teacher_info))
                                            continue;
                                        else
                                            echo "<option value='$id_subject'>$name_subject</option>";
                                    }
                                    echo"
                                </select></label><br /><br />
                               <input type='submit' class='button topic_buttons' name='submit_add_subject' value='Добавить'>
                            </form>
                        </div>";
                    }
                    
                    //модуль добавления предметов в бд
                    $result_subjects_list = mysqli_fetch_all(mysqli_query($link, "SELECT `id`, `name` FROM `Subjects` ORDER BY `name`"));
                    $count_subjects = count($result_subjects_list);
                    echo "
                    <div id='add_subject_block'>
                        <div id='button_close1'></div>
                            <form class='add_subject_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <label>Добавить предмет в базу данных:<br /><br />
                                <input type='text' class='add_subject_name_field' pattern='^[a-zA-Zа-яА-Я0-9\s-]+$' name='add_subject_name' title='Используйте только русские буквы или цифры!' placeholder='Название' required></label><br /><br />
                               <input type='submit' class='button topic_buttons' name='submit_add_subject_to_db' value='Добавить'><br />
                            </form>
                            <hr class='my_hr' />
                            <form class='add_subject_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <input type='hidden' name='teacher_id' value='$result[id]'>
                                <label>Предметы в базе данных:<br />
                                <select class='create_relation_select' size='$count_subjects' name='delete_id_subject_from_bd' required>";
                                    for ($i = 0; $i < $count_subjects; $i++){
                                        $name_subject = $result_subjects_list[$i][1];
                                        $id_subject = $result_subjects_list[$i][0];


                                        if (in_array($id_subject, $result_teacher_info))
                                            continue;
                                        else
                                            echo "<option value='$id_subject'>$name_subject</option>";
                                    }
                                    echo"
                                </select></label><br /><br />
                               <input type='submit' class='button topic_buttons' name='submit_delete_subject_from_db' value='Удалить'>
                            </form>
                    </div>"; 
                }

                if ($result_my_group_forum[group_forum] == 5)
                    echo "<div class='infoblock infoblock_lk_page'>Вы не можете просматривать профили других пользователей. Ваш аккаунт заблокирован!<br> Свяжитесь с администратором.</div>";
                else{
                    echo "
                    <div class='lk_main'>
                        <div class='online_block'>"; 
                            echo show_online($result[id]);
                        echo "
                        </div>
                        <div class='lk_left_side_block'>";
                            if ($result[avatar_folder] != 0)
                                echo "<div class='lk_photo' style='background-image: url(resource/avatar/$result[avatar_folder]/$result[id].jpg); background-size: cover;'></div>";
                            else
                                echo "<div class='lk_photo' style='background: url(resource/avatar/all.png); background-size: cover; background-position: center;'></div>";                   
                            echo "
                            <form class='lk_buttons_block' action='$_SERVER[REQUEST_URI]' method='POST'>
                                <input type='hidden' name='id_user' value='$result[id]'>
                                <div class='lk_default_buttons_block'>
                                    <input type='submit' class='button lk_buttons' name='submit_go_dialog' value='Отправить сообщение'>
                                </div>";          
                                if ($result_my_group_forum[group_forum] == 2){
                                    echo"              
                                    <div class='lk_admin_buttons_block'>
                                        <div class='lk_admin_panel_text'>Панель администратора</div>";
                                            if ($result[group_real] == 3)
                                                echo "<input type='button' id='create_relation_button' class='button lk_buttons' name='' value='Привязка к студентам'>";
                                            else if ($result[group_real] == 2){
                                                if ($result_student_info[chief] != 1)
                                                    echo "<input type='submit' class='button lk_buttons' name='submit_appoint_chief' value='Назначить старостой'>";
                                                else
                                                    echo "<input type='submit' class='button lk_buttons' name='submit_disappoint_chief' value='Снять с должности старосты'>";
                                            }
                                            else if ($result[group_real] == 1 || $result[group_real] == 4){
                                                echo "<input type='button' id='create_subject_button' class='button lk_buttons' name='' value='Предметы преподавателя'>";
                                            }
                                            echo "<input type='button' id='add_subject_button' class='button lk_buttons' name='' value='Предметы в базе данных'>";
                                            if ($result[group_forum] != 2 && $result[group_forum] != 3){
                                                echo"
                                                <input type='submit' class='button lk_buttons' name='submit_appoint_admin' value='Назначить администратором'>
                                                <input type='submit' class='button lk_buttons' name='submit_appoint_moderator' value='Назначить модератором форума'>";
                                            }
                                            else
                                                echo "<input type='submit' class='button lk_buttons' name='submit_disappoint_admin' value='Лишить прав администратора/модератора'>";
                                            if ($result[group_forum] != 5)
                                                echo "<input type='submit' class='button lk_buttons' name='submit_user_ban' value='Заблокировать пользователя'>";
                                            else
                                                echo "<input type='submit' class='button lk_buttons' name='submit_user_ban_off' value='Разблокировать пользователя'>";
                                            echo "<input type='submit' class='button lk_buttons' name='submit_delete_all_user_messages' value='Удалить все сообщения и темы пользователя'>
                                    </div>";
                                }
                            echo"    
                            </form>
                        </div>
                        <div class='lk_info'>
                            <b>id:</b> $result[id]<br/>
                            <hr class='main_lk_hr'>
                            <b>Имя:</b> $result[name]<br/>
                            <hr class='main_lk_hr'>
                            <b>Дата регистрации:</b> $result[registration_date]<br/>";
                            if ($result_settings[show_email] == 1)
                                echo "
                                <hr class='main_lk_hr'>
                                <b>E-mail:</b> <a href='mailto:$result[email]'>$result[email]</a><br/>";
                            if ($result_settings[show_tel] == 1)        
                                echo "
                                <hr class='main_lk_hr'>
                                <b>Номер телефона:</b> $result[phone_number]<br/>";
                            echo"
                            <hr class='main_lk_hr'>
                            <b>Возраст:</b> $result[age]<br/>
                            <hr class='main_lk_hr'>
                            <b>Сообщений на форуме:</b> $result[messages]<br/>
                            <hr class='main_lk_hr'>
                            <b>Созданных тем:</b> $result[topics]<br/>
                            <hr class='main_lk_hr'>
                            <b>Статус:</b> <span style='color:red'>$result_group_forum[name]</span><br/>
                            <hr class='main_lk_hr'>
                            <b>Должность:</b> $result_group_real[name]<br/>
                            <hr class='main_lk_hr'>";
                            if ($result[group_real] == 2){
                                if ($result_student_info[chief] == 1)
                                    $chief = "Да";
                                else 
                                    $chief = "Нет";
                                echo "
                                    <b>Курс:</b> $result_student_info[course]<br/>
                                    <hr class='main_lk_hr'>
                                    <b>Группа:</b> $result_student_info[group]<br/>
                                    <hr class='main_lk_hr'>
                                    <b>Староста:</b> $chief<br/>
                                    <hr class='main_lk_hr'>    
                                ";                                   
                            }
                            else if ($result[group_real] == 3){
                                if ($result_parent_info[0][0] != 0){
                                    echo "<b>Связан со студентом: </b>";
                                    for ($i = 0; $i < count($result_parent_info); $i++){
                                        $link_student = $result_parent_info[$i][0];
                                        $result_link_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Users` WHERE `id` = '$link_student'"));           
                                        echo "<a href='forum.php?id=lk&user=$link_student'>$result_link_student_info[name]<br/>";
                                    }
                                    echo "<hr class='main_lk_hr'>";
                                }
                                else 
                                    echo "<b>Связан со студентом:</b> Нет<br/><hr class='main_lk_hr'>";                                  
                            } 
                            else if ($result[group_real] == 1 || $result[group_real] == 4){
                                if ($result_teacher_info[0][0] != 0){
                                    echo "<b>Предмет: </b>";
                                    for ($i = 0; $i < count($result_teacher_info); $i++){
                                        $subject = $result_teacher_info[$i][1];
                                        echo "$subject<br/>";
                                    }
                                    echo "<hr class='main_lk_hr'>";
                                }
                                else 
                                    echo "<b>Предмет:</b> Нет<br/><hr class='main_lk_hr'>";                                  
                            }
                        echo"
                        </div>
                    </div>";
                }                  
            }
        }
    }
    else
        echo "<div class='infoblock infoblock_lk'>Пожалуйста, авторизуйтесь!</div>";     
?>
