<?php
    if ($_SESSION["vhod"]){
        $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum` FROM `Users` WHERE `id`=$_SESSION[id]"));
        getLkMenu("edit");
        if ($result_group_forum[group_forum] == 5){
            echo "<div class='infoblock infoblock_lk infoblock_lk_edit'>Вы не можете редактировать личные данные. Ваш аккаунт заблокирован!<br> Свяжитесь с администратором.</div>";
        }
        else{
            $result_user_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT Users.name, Users.email, Users.phone_number, Users.age, Settings.show_email, Settings.show_tel FROM `Users` JOIN `Settings` ON Users.id = Settings.user_id WHERE Users.id=$_SESSION[id]"));
            if ($result_user_info[show_email] == 1)
                $show_email = 'checked';
            if ($result_user_info[show_tel] == 1)
                $show_tel = 'checked';
            echo "
            <div class='lk_main'>
                <form class='lk_edit_form' action='$_SERVER[REQUEST_URI]' method='POST' enctype='multipart/form-data'>
                    <h3>Измените ваши данные по желанию</h3><br /><br />
                    <label>Сменить фотографию<br \><input type='file' name='avatar'></label><br /><br />
                    <hr class='main_lk_hr'><br />
                    <label>Сменить пароль<br />
                    <input class='lk_edit_form_field' type='password' name='password' pattern='[a-zA-Z0-9]{5,20}' title='Латинские буквы или цифры, не менее 5, не более 20, без пробелов' placeholder='Введите новый пароль'><br />
                    <input class='lk_edit_form_field' type='password' name='password_2' pattern='[a-zA-Z0-9]{5,20}' title='Латинские буквы или цифры, не менее 5, не более 20, без пробелов' placeholder='Введите новый пароль еще раз'></label><br /><br /><br />
                    <hr class='main_lk_hr'><br />
                    <label>Изменить ФИО<br /><input class='lk_edit_form_field' type='text' name='name' pattern='[а-яА-Я\-]{1,30}[\s]{1}[а-яА-Я]{1,30}[\s]{1}[а-яА-Я]{1,30}' title='Необходимо ввести на русском языке данные в формате:Фамилия Имя Отчество' placeholder='Новое ФИО' value='$result_user_info[name]'></label><br /><br /><br />
                    <hr class='main_lk_hr'><br />
                    <label>Изменить электронную почту<br /><input class='lk_edit_form_field' type='email' name='email' maxlength='50' placeholder='Новый E-mail(Электронная почта)' value='$result_user_info[email]'></label><br /><br /><br />
                    <hr class='main_lk_hr'><br />
                    <label>Изменить телефон<br /><input class='lk_edit_form_field' type='tel' name='phone_number' maxlength='15' placeholder='Новый телефон' value='$result_user_info[phone_number]'></label><br /><br /><br />
                    <hr class='main_lk_hr'><br />
                    <label>Изменить возраст<br /><input class='lk_edit_form_field' type='number' name='age' min='15' max='150' value='$result_user_info[age]'></label><br /><br /><br />
                    <hr class='main_lk_hr'><br />
                    <div class='reg_check' style='display: inline-block; text-align:left;'>
                        <label><input type='checkbox' name='edit_show_email' value='1' $show_email> Скрывать E-Mail от других пользователей?</label><br/><br/>
                        <label><input type='checkbox' name='edit_show_tel' value='1' $show_tel> Скрывать телефон от других пользователей?</label><br/>
                    </div><br /><br />
                    <input class='button' type='reset'>
                    <input class='button' name='submit_lk_edit' type='submit' value='Сохранить'> 

                </form>
                <div class='lk_edit_delete_page_block'>
                    <p>Вы можете <a href='forum.php?id=lk&lk_page=delete_account'>удалить свой аккаунт</a></p>
                </div>
            </div>
            ";
        }
    }
    else
        echo "<div class='infoblock infoblock_lk'>Пожалуйста, авторизуйтесь!</div>"; 
?>

