<?php 
if ($_SESSION["vhod"]){   
    $result_message_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `id_author`, `text` FROM `Messages` WHERE `id`=$message"));
    $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum` FROM `Users` WHERE `id`=$_SESSION[id]"));
    if ($result_group_forum[group_forum] == 5)
        echo "<div class='infoblock infoblock_lk'>Вы не можете редактировать сообщения. Ваш аккаунт заблокирован! Свяжитесь с администратором.</div>";
    else if ($result_group_forum[group_forum] == 2 || $result_group_forum[group_forum] == 3 || $result_message_info[id_author] == $_SESSION[id]){
        echo<<<SHOW
        <form class="edit_message_form" action="$_SERVER[REQUEST_URI]" method="POST">
            <input type='hidden' name='message_edit_id' value='$message'>
            <label>Редактировать сообщение:<br /><br />             
                <textarea class='edit_message_textarea' maxlength='10000' name='message_edit_text' required>$result_message_info[text]</textarea></label><br /><br />
            </label>
            <input type="submit" class='button' name="submit_message_edit" value="Сохранить">
        </form>
SHOW;
    }
    else
        echo "<div class='infoblock infoblock_lk'>У вас нет полномочий для изменения данного сообщения!</div>";
}
else
    echo "<div class='infoblock infoblock_lk'>Пожалуйста, авторизуйтесь!</div>";
?>
