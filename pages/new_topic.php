<?php 
if ($_SESSION["vhod"]){   
    $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum`, `active` FROM `Users` WHERE `id`=$_SESSION[id]"));
    if ($result_group_forum[group_forum] == 5)
        echo "<div class='infoblock infoblock_lk'>Вы не можете создавать новые темы. Ваш аккаунт заблокирован!<br> Свяжитесь с администратором.</div>";
    else if ($result_group_forum[active] == 0)
        echo "<div class='infoblock infoblock_lk'>Вы не можете создавать новые темы. Ваш аккаунт не активирован по e-mail!</div>";
    else
        echo<<<SHOW
        <form class="new_topic_form" action="$_SERVER[REQUEST_URI]" method="POST">
            <label>Название темы<br /><input type="text" class="new_topic_field" name="topic_name" maxlength="100" required></label><br />
            <label>Текст сообщения <br />
            <textarea class="new_topic_textarea" maxlength="10000" name="text" cols="121" rows="20" required></textarea></label><br />
            <input type="submit" class='button' name="submit_new_topic" value="Создать">
        </form>
SHOW;
}
else
    echo "<div class='infoblock infoblock_lk'>Пожалуйста, авторизуйтесь!</div>";
?>