<form class="reg_form" action="<?= $_SERVER[REQUEST_URI]?>" method="POST">
    <h1>Регистрация</h1>
    <div class='reg_fields_block'>
        <label><select class="field" size="1" name="group_real">
            <option value="1">Преподаватель</option>
            <option selected value="2">Студент</option>
            <option value="3">Родитель студента</option>
        </select>Выберите должность</label><br/>
        <label><input class="field" type="text" name="login" pattern="[a-zA-Z0-9]{3,20}" title='Латинские буквы или цифры, не менее 3, не более 20, без пробелов' placeholder="Логин" required>Введите логин</label><br/>
        <label><input class="field" type="password" name="password" pattern="[a-zA-Z0-9]{5,20}" title='Латинские буквы или цифры, не менее 5, не более 20, без пробелов' placeholder="Пароль" required>Введите пароль</label><br/>
        <label><input class="field" type="text" name="name" pattern="[а-яА-Я\-]{1,30}[\s]{1}[а-яА-Я]{1,30}[\s]{1}[а-яА-Я]{1,30}" title='Необходимо ввести на русском языке данные в формате:Фамилия Имя Отчество' placeholder="ФИО" required>Введите ФИО</label><br/>
        <label><input class="field" type="email" name="email" maxlength="50" placeholder="E-mail(Электронная почта)" required>Введите электронную почту</label><br/>
        <label><input class="field" type="tel" name="phone_number" maxlength="15" placeholder="Телефон">Введите телефон</label><br/>
        <label><input class="field" type="number" name="age" min="15" max="150" placeholder="Возраст" required>Введите ваш возраст</label><br/>

        <div id='reg_options'>
            <div id='div2' style='display:inline-block;'>
                <label><input class="field" type="number" name="course" min="1" max="6" placeholder="Курс">Введите ваш номер курса</label><br/>
                <label><input class="field" type="number" name="group" min="1" placeholder="Номер группы">Введите номер группы в которой учитесь</label><br/><br/>
            </div>
            <?php 
            $result_subject_list = mysqli_fetch_all(mysqli_query($link, "SELECT `id`,`name` FROM `Subjects` ORDER BY `name`"));
            ?>
            <div id='div1' style='display:none;'>
                <label><select class="field" size="1" name="subjects" style='width: auto;'>
                    <?php 
                    for ($i = 0; $i < count($result_subject_list); $i++){
                        $name_subject = $result_subject_list[$i][1];
                        $id_subject = $result_subject_list[$i][0];
                        echo "<option value='$id_subject'>$name_subject</option>";
                    }
                    ?>
                </select>Выберите предмет</label><br/>           
            </div>
        </div>
        <div class='reg_check'>
            <label><input type='checkbox' name='show_email' value='1'> Скрывать E-Mail от других пользователей?</label><br/><br/>
            <label><input type='checkbox' name='show_tel' value='1'> Скрывать телефон от других пользователей?</label><br/>
        </div>
        <div class='reg_check'>
            <img src='../resource/captcha.php' alt="Каптча"><br/>
            <label><input class="field" type="text" name="captcha" placeholder="Текст с картинки" required>Введите то, что видите на картинке</label><br/>
        </div>
        
        <div class="reg_buttons">
            <input class="button" id='reg_reset_button' type="reset">
            <input class="button" name="submit_reg" type="submit" value="Готово"> 
        </div>
    </div>
</form>

