<?php
if ($_SESSION["vhod"]){
    getLkMenu("show_grades");
    if ($_SESSION[grades_type] == 'Журнал')
        $select_journal = 'selected';
    else if ($_SESSION[grades_type] == 'Аттестация')
        $select_attestation = 'selected';
    $result = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum`, `group_real`  FROM `Users` WHERE `id`=$_SESSION[id]"));
    if ($result[group_forum] == 5){//если пользователь заблокирован
        echo "<div class='infoblock infoblock_lk infoblock_lk_edit'>Вы не можете просматривать данную страницу. Ваш аккаунт заблокирован!<br> Свяжитесь с администратором.</div>";
    }
    else if ($result[group_real] != 3)//если пользователь не родитель
        echo "<div class='infoblock infoblock_lk infoblock_lk_edit'>У вас нет прав доступа для просмотра данной страницы, т.к. вы не являетесь родителем студента.</div>";
    else{
        $result_students_list = mysqli_fetch_all(mysqli_query($link, "SELECT `link_student` FROM `Parents` WHERE `id_user`=$_SESSION[id]"));
        echo "
        <div class='lk_main'>
            <form class='users_list_form' action='$_SERVER[REQUEST_URI]' method='POST'>
                <h3>Просмотр успеваемости</h3><br />";
                
                if (count($result_students_list) > 1){
                    echo"
                    <div class='chief_page_form_part'>
                        <label>Студент<br />
                        <select class='chief_page_form_field' style='width: auto' size='1' name='link_student'>";
                            for ($i = 0; $i < count($result_students_list); $i++){
                                $link_student = $result_students_list[$i][0];
                                $result_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Users` WHERE `id`= '$link_student'"));

                                if ($link_student == $_SESSION[link_student]){
                                    $selected = 'selected';
                                }
                                echo "<option $selected value='$link_student'>$result_student_info[name]</option>";
                                $selected = '';
                            }
                            echo"
                        </select></label>
                    </div>";
                }
                else{
                    $link_student = $result_students_list[0][0];
                    echo "<input type='hidden' name='link_student' value='$link_student'>";
                }
                
                echo"
                <div class='chief_page_form_part'>
                    <label>Документ<br />
                    <select class='chief_page_form_field' style='width: auto' size='1' name='grades_type'>
                            <option $select_journal value='1'>Журнал посещаемости</option>
                            <option $select_attestation value='2'>Аттестация</option>";
                        echo"
                    </select></label>
                </div>";
                echo"
                <div class='users_list_form_button'>
                    <input class='button' name='submit_show_grades' type='submit' value='Показать'>  
                </div>
            </form>";   
        if ($_SESSION[show_grades] == "Yes"){  
            if ($result_students_list == NULL)
                echo "<p> Данные не найдены, т.к. вы не привязаны к студенту, обратитесь к администратору!</p>";
            else{
                if ($_SESSION[link_student] > 0){
                    if ($_SESSION[grades_type] == 'Журнал'){
                        $link_student = $_SESSION[link_student];

                        //Поиск группы студента
                        $result_student_group = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group` FROM `Students` WHERE `id_user`= '$link_student'"));
                        $journal_name = "Journal_".$result_student_group[group];

                        $sql = "SHOW TABLES LIKE '$journal_name'";
                        $query = mysqli_query($link, $sql);
                        $result_tables = mysqli_fetch_all($query);

                        if ($result_tables == NULL){
                                echo "<p> Журналы посещаемости не загружены, обратитесь к старосте группы $result_student_group[group].</p>";             
                        }
                        else{
                            $result_show_grades = mysqli_fetch_all(mysqli_query($link, "SELECT `week`, `date`, `name`, `lesson_1` , `is_1`, `lesson_2` , `is_2`, `lesson_3` , `is_3`, `lesson_4` , `is_4` FROM `$journal_name` WHERE `student_id`= '$link_student'"));

                            if ($result_show_grades != NULL){
                                echo "
                                <table class='journal_table'>
                                    <tr>
                                        <th>Неделя</th>
                                        <th>Дата</th>
                                        <th>Студент</th>
                                        <th>1 пара</th>
                                        <th>Был</th>
                                        <th>2 пара</th>
                                        <th>Был</th>
                                        <th>3 пара</th>
                                        <th>Был</th>
                                        <th>4 пара</th>
                                        <th>Был</th>
                                    </tr>
                                        <col width='5%'>
                                        <col width='auto'>
                                        <col width='auto'>
                                        <col width='auto'>
                                        <col width='5%'>
                                        <col width='auto'>
                                        <col width='5%'>
                                        <col width='auto'>
                                        <col width='5%'>
                                        <col width='auto'>
                                        <col width='5%'>";
                                for ($i = 0; $i < count($result_show_grades); $i++){
                                    $week = $result_show_grades[$i][0];
                                    $date = $result_show_grades[$i][1];
                                    $name = $result_show_grades[$i][2];

                                    if ($result_show_grades[$i][4] == 1){
                                        $lesson_1 = ">".$result_show_grades[$i][3];
                                        $is_1 = '>Да';
                                    }
                                    else{
                                        $lesson_1 = " style=\"color: red;\">".$result_show_grades[$i][3];
                                        if ($result_show_grades[$i][4] == 0)
                                            $is_1 = ' style=\'color: red;\'>Нет';
                                        else 
                                            $is_1 = '>';
                                    }

                                    if ($result_show_grades[$i][6] == 1){
                                        $lesson_2 = ">".$result_show_grades[$i][5];
                                        $is_2 = '>Да';
                                    }
                                    else{
                                        $lesson_2 = " style=\"color: red;\">".$result_show_grades[$i][5];
                                        if ($result_show_grades[$i][6] == 0)
                                            $is_2 = ' style=\'color: red;\'>Нет';
                                        else 
                                            $is_2 = '>';
                                    }

                                    if ($result_show_grades[$i][8] == 1){
                                        $lesson_3 = ">".$result_show_grades[$i][7];
                                        $is_3 = '>Да';
                                    }
                                    else{
                                        $lesson_3 = " style=\"color: red;\">".$result_show_grades[$i][7];
                                        if ($result_show_grades[$i][8] == 0)
                                            $is_3 = ' style=\'color: red;\'>Нет';
                                        else 
                                            $is_3 = '>';
                                    }

                                    if ($result_show_grades[$i][10] == 1){
                                        $lesson_4 = ">".$result_show_grades[$i][9];
                                        $is_4 = '>Да';
                                    }
                                    else{
                                        $lesson_4 = " style=\"color: red;\">".$result_show_grades[$i][9];
                                        if ($result_show_grades[$i][10] == 0)
                                            $is_4 = ' style=\'color: red;\'>Нет';
                                        else 
                                            $is_4 = '>';
                                    }
                                  echo "
                                    <tr>
                                        <td>$week</td>
                                        <td>$date</td>
                                        <td><a href='forum.php?id=lk&user=$link_student'>$name</a></td>
                                        <td$lesson_1</td>
                                        <td$is_1</td>
                                        <td$lesson_2</td>
                                        <td$is_2</td>
                                        <td$lesson_3</td>
                                        <td$is_3</td>
                                        <td$lesson_4</td>
                                        <td$is_4</td>
                                    </tr>";
                                }            
                                echo "</table>";
                            }
                            else
                                echo "<p> Журналы посещаемости не загружены, обратитесь к старосте группы $result_student_group[group].</p>";
                        }
                    }
                    else if ($_SESSION[grades_type] == 'Аттестация'){
                        $link_student = $_SESSION[link_student];

                        //Поиск группы студента
                        $result_student_group = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group` FROM `Students` WHERE `id_user`= '$link_student'"));
                        $attestation_name = "Attestation_".$result_student_group[group];

                        $sql = "SHOW TABLES LIKE '$attestation_name'";
                        $query = mysqli_query($link, $sql);
                        $result_tables = mysqli_fetch_all($query);

                        if ($result_tables == NULL){
                                echo "<p> Аттестации не загружены, обратитесь к старосте группы $result_student_group[group].</p>";             
                        }
                        else{
                            $result_attestation = mysqli_fetch_all(mysqli_query($link, "SELECT `date`, `student_name`, `student_id`, `teacher_name`, `teacher_id`, `lesson_name`, `progress`, `hours_miss` FROM `$attestation_name` WHERE `student_id`= '$link_student'"));

                            if ($result_attestation != NULL){
                                echo "
                                <table class='journal_table'>
                                    <tr>
                                        <th>Дата</th>
                                        <th>Студент</th>
                                        <th>Преподаватель</th>
                                        <th>Предмет</th>
                                        <th>Успеваемость</th>
                                        <th>Часов пропущено</th>
                                    </tr>";
                                for ($i = 0; $i < count($result_attestation); $i++){
                                    $date = $result_attestation[$i][0];
                                    $student_name = $result_attestation[$i][1];
                                    if ($result_attestation[$i][2] != 0)
                                        $student_id = $result_attestation[$i][2];
                                    $teacher_name = $result_attestation[$i][3];
                                    if ($result_attestation[$i][4] != 0)
                                        $teacher_id = $result_attestation[$i][4];
                                    $lesson_name = $result_attestation[$i][5];

                                    if ($result_attestation[$i][6] != 0){
                                        $progress = ">".$result_attestation[$i][6];
                                    }
                                    else{
                                        $progress = " style=\"color: red;\">".$result_attestation[$i][6];
                                    }

                                    if ($result_attestation[$i][7] == 0){
                                        $hours_miss = ">".$result_attestation[$i][7];
                                    }
                                    else{
                                        $hours_miss = " style=\"color: red;\">".$result_attestation[$i][7];
                                    }

                                    echo "
                                    <tr>
                                        <td>$date</td>";
                                        if ($student_id != 0)
                                            echo "<td><a href='forum.php?id=lk&user=$student_id'>$student_name</a></td>";
                                        else
                                            echo "<td>$student_name</td>";
                                        $student_id = 0;
                                        if ($teacher_id != 0)
                                            echo "<td><a href='forum.php?id=lk&user=$teacher_id'>$teacher_name</a></td>";
                                        else
                                            echo "<td>$teacher_name</td>";
                                        $teacher_id = 0;
                                        echo"
                                        <td>$lesson_name</td>
                                        <td$progress</td>
                                        <td$hours_miss</td>
                                    </tr>";
                                }            
                                echo "</table>";
                            }
                            else
                                echo "<p> Аттестации не загружены, обратитесь к старосте группы $result_student_group[group].</p>";
                        }
                        
                    }
                }
            }
        }
        echo "
        </div>";
    }
}
else
    echo "<div class='infoblock infoblock_lk'>Пожалуйста, авторизуйтесь!</div>"; 
?>  

