<?php 
if ($_SESSION["vhod"]){   
    $result_topic_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `id_author`, `topic_name` FROM `Topics` WHERE `id`=$showtopic"));
    $result_group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `group_forum`, `active` FROM `Users` WHERE `id`=$_SESSION[id]"));
    if ($result_group_forum[active] != 1)
        echo "<div class='infoblock infoblock_lk'>Вы не можете редактировать темы. Ваш аккаунт не активирован по e-mail!</div>";
    else if ($result_group_forum[group_forum] == 5)
        echo "<div class='infoblock infoblock_lk'>Вы не можете редактировать темы. Ваш аккаунт заблокирован! Свяжитесь с администратором.</div>";
    else if ($result_group_forum[group_forum] == 2 || $result_group_forum[group_forum] == 3 || $result_topic_info[id_author] == $_SESSION[id]){
        echo<<<SHOW
        <form class="new_topic_form" action="$_SERVER[REQUEST_URI]" method="POST">
            <input type='hidden' name='id_topic' value='$showtopic'>
            <label>Изменить название темы<br /><input type="text" class="new_topic_field" name="topic_name" maxlength="100" value='$result_topic_info[topic_name]'  required</label><br />
            <input type="submit" class='button' name="submit_edit_topic" value="Сохранить">
        </form>
SHOW;
    }
    else
        echo "<div class='infoblock infoblock_lk'>У вас нет полномочий для изменения данной темы!</div>";
}
else
    echo "<div class='infoblock infoblock_lk'>Пожалуйста, авторизуйтесь!</div>";
?>