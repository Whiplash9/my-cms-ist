<?php
if ($_SESSION["vhod"]){
    getLkMenu("users_list");
    if ($_SESSION[users_list_group_real] == 1){
        $show_one = 'display: inline-block;';
        $one = 'selected';
        $show_two = 'display: none;';
    }
    else if ($_SESSION[users_list_group_real] == 2){
        $show_two = 'display: inline-block;';
        $two = 'selected';
        $show_one = 'display: none;';
    }
    else if ($_SESSION[users_list_group_real] == 3){
        $three = 'selected';
        $show_two = 'display: none;';
        $show_one = 'display: none;';

    }
    else if ($_SESSION[users_list_group_real] == 4){
        $four = 'selected';
        $show_two = 'display: none;';
        $show_one = 'display: none;';
    }
    ?>
    <div class="lk_main">
        <form class='users_list_form' action='<?=$_SERVER[REQUEST_URI]?>' method='POST'>
            <h3>Условия поиска</h3><br />
            <div class='users_list_form_part'>
                <label>Группа пользователя<br />
                <select class='users_list_form_field' size='1' name='group_real'>
                    <option <?=$one?> value="1">Преподаватель</option>
                    <option <?=$two?> value="2">Студент</option>
                    <option <?=$three?> value="3">Родитель студента</option>
                    <option <?=$four?> value="4">Заведующий кафедры</option>
                </select></label>
            </div>  
            <div id='search_options' style='display: inline-block; vertical-align: bottom;'>
                <div id='div2' style='display: none; <?=$show_two?>'>
                    <div class='users_list_form_part'>
                        <label>Курс<br /><input class="users_list_form_field" type="number" name="course" min="1" value='<?=$_SESSION[users_list_course]?>' placeholder="Курс"></label> 
                    </div>
                    <div class='users_list_form_part'>
                        <label>Номер группы<br /><input class="users_list_form_field" type="number" name="group" min="1" value='<?=$_SESSION[users_list_group]?>' placeholder="Номер группы"></label><br/>
                    </div>
                </div>
                <?php 
                $result_subject_list = mysqli_fetch_all(mysqli_query($link, "SELECT `id`,`name` FROM `Subjects` ORDER BY `name`"));
                ?>
                <div id='div1' style='display: inline-block; <?=$show_one?>; vertical-align: top;'>
                    <div class='users_list_form_part'>
                        <label>Выберите предмет<br /><select class="users_list_form_field" size="1" name="search_subjects" style='width: auto;'>
                            <option value='0'>Все предметы</option>    
                            <?php 
                            for ($i = 0; $i < count($result_subject_list); $i++){
                                $name_subject = $result_subject_list[$i][1];
                                $id_subject = $result_subject_list[$i][0];
                                if ($id_subject == $_SESSION[users_list_subject])
                                    $subject = 'selected';
                                else
                                    $subject = '';
                                echo "<option value='$id_subject' $subject>$name_subject</option>";
                            }
                            ?>
                        </select></label><br/>  
                    </div>
                </div>
            </div>

            <div class='users_list_form_button'>
                <input class='button' name='submit_lk_users_list' type='submit' value='Поиск'>  
            </div>
        </form>
    <?php
        if ($_SESSION[users_list_group_real]){
            switch ($_SESSION[users_list_group_real]){
                case 1:
                    if ($_SESSION[users_list_subject] != 0)
                        $result_users_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.id, Users.name, Users.group_forum, Teachers.subject FROM `Users` JOIN `Teachers` ON Users.id = Teachers.id_user WHERE Teachers.subject = '$_SESSION[users_list_subject]'"));
                    else 
                        $result_users_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.id, Users.name, Users.group_forum, Teachers.subject FROM `Users` JOIN `Teachers` ON Users.id = Teachers.id_user"));
                    if ($result_users_list == NULL)
                        echo "<div class = 'users_list_not_found'>Поиск не дал результатов!</div>";
                    else{
                        echo "
                        <table class='users_list_table'>
                            <tr>
                                <th>ФИО</th>
                                <th>Предмет</th>
                                <th>Полномочия</th>
                            </tr>";
                        for ($i = 0; $i < count($result_users_list); $i++){
                            $id = $result_users_list[$i][0];
                            $name = $result_users_list[$i][1];
                            $group_forum = $result_users_list[$i][2];
                            $subject = $result_users_list[$i][3];
                            $result_subject_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Subjects` WHERE id = '$subject'"));  
                            $result_group_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Group_forum` WHERE id = '$group_forum'"));  
                            echo "
                            <tr>
                                <td><a href='forum.php?id=lk&user=$id'>$name</a></td>
                                <td>$result_subject_info[name]</td>
                                <td>$result_group_info[name]</td>
                            </tr>";
                        }
                    }              
                    break;
                case 2:
                    if ($_SESSION[users_list_course] != 0 && $_SESSION[users_list_group] != 0)
                        $result_users_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.id, Users.name, Users.group_forum, Students.course, Students.group FROM `Users` JOIN `Students` ON Users.id = Students.id_user WHERE Users.group_real = '$_SESSION[users_list_group_real]' AND (Students.course = '$_SESSION[users_list_course]' AND Students.group = '$_SESSION[users_list_group]')"));
                    else if ($_SESSION[users_list_course] != 0 || $_SESSION[users_list_group] != 0)
                        $result_users_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.id, Users.name, Users.group_forum, Students.course, Students.group FROM `Users` JOIN `Students` ON Users.id = Students.id_user WHERE Users.group_real = '$_SESSION[users_list_group_real]' AND (Students.course = '$_SESSION[users_list_course]' OR Students.group = '$_SESSION[users_list_group]')"));
                    else
                        $result_users_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.id, Users.name, Group_forum.name FROM `Users` JOIN `Group_forum` ON Users.group_forum = Group_forum.id WHERE Users.group_real='$_SESSION[users_list_group_real]'"));

                    if ($result_users_list == NULL)
                        echo "<div class = 'users_list_not_found'>Поиск не дал результатов!</div>";
                    else{
                        echo "
                        <table class='users_list_table'>
                            <tr>
                                <th>ФИО</th>
                                <th>Курс</th>
                                <th>Группа</th>
                                <th>Староста</th>
                                <th>Полномочия</th>
                            </tr>";
                        for ($i = 0; $i < count($result_users_list); $i++){
                            $id = $result_users_list[$i][0];
                            $name = $result_users_list[$i][1];
                            $group_forum = $result_users_list[$i][2];
                            if ($_SESSION[users_list_course] != 0 || $_SESSION[users_list_group] != 0){
                                $group_forum = mysqli_fetch_assoc(mysqli_query($link, "SELECT `name` FROM `Group_forum` WHERE `id`='$group_forum'"));
                                $group_forum = $group_forum[name];
                            }
                            $result_student_info = mysqli_fetch_assoc(mysqli_query($link, "SELECT `course`, `group`, `chief` FROM `Students` WHERE `id_user`='$id'"));           
                            if ($result_student_info[chief] == 1)
                                $chief = 'Да';
                            else 
                                $chief = 'Нет';
                            echo "
                            <tr>
                                <td><a href='forum.php?id=lk&user=$id'>$name</a></td>
                                <td>$result_student_info[course]</td>
                                <td>$result_student_info[group]</td>
                                <td>$chief</td>
                                <td>$group_forum</td>
                            </tr>";
                        }
                    }
                    break;
                case 3:
                    $result_users_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.id, Users.name, Group_forum.name FROM `Users` JOIN `Group_forum` ON Users.group_forum = Group_forum.id WHERE Users.group_real='$_SESSION[users_list_group_real]'"));
                    if ($result_users_list == NULL)
                        echo "<div class = 'users_list_not_found'>Поиск не дал результатов!</div>";
                    else{
                        echo "
                        <table class='users_list_table'>
                            <tr>
                                <th>ФИО</th>
                                <th>Привязан к студенту</th>
                                <th>Полномочия</th>
                            </tr>";
                        for ($i = 0; $i < count($result_users_list); $i++){
                            $id = $result_users_list[$i][0];
                            $name = $result_users_list[$i][1];
                            $group_forum = $result_users_list[$i][2];
                            $result_link_student_info = mysqli_fetch_all(mysqli_query($link, "SELECT Users.name, Users.id FROM `Parents` JOIN `Users` ON Parents.link_student = Users.id WHERE Parents.id_user='$id'"));           

                            echo "
                            <tr>
                                <td><a href='forum.php?id=lk&user=$id'>$name</a></td>";
                                if ($result_link_student_info != NULL){
                                    echo "<td>";
                                    for ($k = 0; $k < count($result_link_student_info); $k++){
                                        $link_student_id = $result_link_student_info[$k][1];
                                        $link_student_name = $result_link_student_info[$k][0];
                                        echo "<a href='forum.php?id=lk&user=$link_student_id'>$link_student_name</a><br />";
                                    }
                                    echo "</td>";
                                }
                                else
                                    echo "<td>Нет</td>";
                                echo"
                                <td>$group_forum</td>
                            </tr>";
                        }  
                    }
                    break;
                case 4:
                    $result_users_list = mysqli_fetch_all(mysqli_query($link, "SELECT Users.id, Users.name, Group_forum.name FROM `Users` JOIN `Group_forum` ON Users.group_forum = Group_forum.id WHERE Users.group_real='$_SESSION[users_list_group_real]'"));
                    if ($result_users_list == NULL)
                        echo "<div class = 'users_list_not_found'>Поиск не дал результатов!</div>";
                    else{
                        echo "
                        <table class='users_list_table'>
                            <tr>
                                <th>ФИО</th>
                                <th>Полномочия</th>
                            </tr>";
                        for ($i = 0; $i < count($result_users_list); $i++){
                            $id = $result_users_list[$i][0];
                            $name = $result_users_list[$i][1];
                            $group_forum = $result_users_list[$i][2];
                            echo "
                            <tr>
                                <td><a href='forum.php?id=lk&user=$id'>$name</a></td>
                                <td>$group_forum</td>
                            </tr>";
                        }
                    }
                    break;
            }
            echo "</table>";
        }
    echo "
    </div>";
}
else
    echo "<div class='infoblock infoblock_lk'>Пожалуйста, авторизуйтесь!</div>"; 
?>  
