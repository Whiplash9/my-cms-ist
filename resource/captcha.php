<?php
    session_start();
    $random = rand(10001, 99999);
    unset($_SESSION['captcha']);
    $_SESSION['captcha'] = md5($random);
    
    $image = imagecreatetruecolor(280, 80);
    imagefilledrectangle($image, 0, 0, 280, 80, imagecolorallocate($image, 255, 255, 255));
    imagettftext($image, 40, 10, 70, 70, imagecolorallocate($image, 82, 82, 82), 'fonts/captcha.ttf', $random);
    header('Expires: Wed, 1 Jan 1997 00:00:00 GMT');
    header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    header('Cache-Control: no-store, no-cache, must-revalidate');
    header('Cache-Control: post-check=0, pre-check=0', false);
    header('Pragma: no-cache');
    header ('Content-type: image/gif');
    imagegif($image);
    imagedestroy($image);

