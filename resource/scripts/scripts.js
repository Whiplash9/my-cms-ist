$(document).ready(function(){
    //создание новостей
    $('#new_news_button').click(function(){
        $('#new_news_block').show();
        $('#overlay').show();
    });
    $('#button_close').click(function(){
        $('#new_news_block').hide();
        $('#overlay').hide();
    });
    
    //создание связи родителя и студента
    $('#create_relation_button').click(function(){
        $('#create_relation_block').show();
        $('#overlay').show();
    });
    $('#button_close').click(function(){
        $('#create_relation_block').hide();
        $('#overlay').hide();
    });
    
    //добавление предметов преподавателю
    $('#create_subject_button').click(function(){
        $('#create_relation_block').show();
        $('#overlay').show();
    });
    $('#button_close').click(function(){
        $('#create_relation_block').hide();
        $('#overlay').hide();
    });
    
    //добавление предметов в бд
    $('#add_subject_button').click(function(){
        $('#add_subject_block').show();
        $('#overlay').show();
    });
    $('#button_close').click(function(){
        $('#add_subject_block').hide();
        $('#overlay').hide();
    });
    $('#button_close1').click(function(){
        $('#add_subject_block').hide();
        $('#overlay').hide();
    });
    
    
    //Регистрация.загрузка по селектору
    $('select[name="group_real"]').change(function(){
    var el = $(this).val();
        $('#reg_options div').css('display','none');
        $('#div'+el).css('display','block');
    });
    $('#reg_reset_button').click(function(){
        $('#div1').hide();
    });
    $('#reg_reset_button').click(function(){
        $('#div2').show();
    });
    
    //Поиск пользователей
    $('select[name="group_real"]').change(function(){
    var el = $(this).val();
        $('#search_options>div').css('display','none');
        $('#div'+el).css('display','inline-block');
    });
});

